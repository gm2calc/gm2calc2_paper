#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.interpolate

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def get_data(filename):
    m, an, dan, ar, dar = np.genfromtxt(filename, unpack=True, dtype=float)
    return m, an*1e10, dan*1e10, ar*1e10, dar*1e10


def get_thdmc(filename):
    m, a = np.genfromtxt(filename, unpack=True, dtype=float)
    SM1L = 2.21595610667419971e-14  # 1-loop contribution from hSM
    SM2L = -1.64007440622957403e-11 # 2-loop contribution from hSM
    char = 7.22319329847628356e-12  # average charged Higgs contribution
    # 2HDMC does not substact the 1- and 2-loop hSM contributions, so we subtract them.
    # 2HDMC does not include the charged Higgs contributions, so we add them.
    return m, (a + char - SM1L - SM2L)*1e10


def plot(datafile, outputfile):
    m, an, dan, ar, dar = get_data(datafile)
    fig = plt.figure(figsize=(4,4))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='normal')
    plt.xlabel(r"$m_{A}$ / GeV")
    plt.ylabel(r"$a_\mu^{\text{F}} \times 10^{10}$")
    plt.tight_layout()

    # 2HDMC data
    m_thdmc, a_thdmc = get_thdmc(r'2hdmc-scan.txt')

    plt.xlim(np.min(m), np.max(m))
    plt.grid(linestyle=':', linewidth=0.5)
    plt.plot(m, ar, 'r-', label=r"running masses")
    plt.plot(m, an, 'g--', label=r"input masses")
    plt.plot(m_thdmc, a_thdmc, 'k:', label=r"2HDMC*")
    plt.fill_between(m, ar + dar, ar - dar, facecolor='r', alpha=0.1)
    plt.fill_between(m, an + dan, an - dan, facecolor='g', alpha=0.1)
    legend = plt.legend(loc='upper right', framealpha=1, edgecolor='k')
    legend.set_title("type II")

    plt.savefig(outputfile)
    plt.close()


plot(r'running.txt', r'running.pdf')
