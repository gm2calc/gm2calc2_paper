/* Perform scan over mA with 2HDMC

   The results of this scan can be nicely reproduced with GM2Calc, if
   the following changes/settings are applied to GM2Calc:

   * "running couplings" must be enabled in GM2Calc

   * The bosonic 2-loop contributions must be disabled.
     -> What remains are only 2-loop fermionic contributions.

   * The 2-loop fermionic contribution with a charged Higgs must be
     disabled.

   * The 2-loop fermionic contribution with the Standard Model Higgs
     must *not* be subtracted.

   2HDMC includes only the following contributions: fermionic 2-loop
   with h, H and A Higgs bosons.  In particular contributions that
   involve the H^+ Higgs boson are not included.  Furthermore, the SM
   contribution is *not* subtracted.

 */

#include "Constraints.h"
#include "DecayTable.h"
#include "HBHS.h"
#include "SM.h"
#include "THDM.h"
#include <iostream>

using namespace std;

/*
 To generate the 2-loop 2HDM contributions with this 2HDMC program,
 uncomment the 1-loop contributions in 2HDMC:

--- a/src/Constraints.cpp
+++ b/src/Constraints.cpp
@@ -470,7 +470,7 @@ double Constraints::delta_amu() {
 
   }
 
-  damutot = damu_1loop+damu_2loop;
+  damutot = damu_2loop;
 
   return damutot;
 }

*/

int main(int argc, char *argv[]) {

  const double m_start = 100.0;
  const double m_stop = 500.0;
  const int N_steps = 200;

  printf("%-17s\t%-17s\n", "# mA", "amu(2L)");

  for (int i = 0; i <= N_steps; ++i) {
     SM sm;
     sm.set_qmass_pole(6, 173.34);
     sm.set_qmass_pole(5, 4.75);
     sm.set_qmass_pole(4, 1.42);
     sm.set_lmass_pole(3, 1.77684);
     sm.set_alpha(1. / 127.934);
     sm.set_alpha0(1. / 137.0359997);
     sm.set_alpha_s(0.1184);
     sm.set_MZ(91.1876);
     sm.set_MW(80.385);
     sm.set_gamma_Z(2.49581);
     sm.set_gamma_W(2.08856);
     sm.set_GF(1.16637E-5);

     THDM model;
     model.set_SM(sm);

     const double mh = 125.;
     const double mH = 400.;
     const double mA = m_start + double(i)*(m_stop - m_start)/N_steps;
     const double mC = 440.;
     const double sba = 0.999;
     const double lambda_6 = 0.;
     const double lambda_7 = 0.;
     const double m12_2 = 40000.;
     const double tb = 3.;

     bool pset = model.set_param_phys(mh, mH, mA, mC, sba, lambda_6, lambda_7, m12_2, tb);

     if (!pset) {
        cerr << "The specified parameters are not valid\n";
        return -1;
     }

     model.set_yukawas_type(2);

     // // Print the parameters in different parametrizations
     // model.print_param_phys();
     // model.print_param_gen();
     // model.print_param_higgs();
     // model.print_param_hybrid();

     Constraints constr(model);
     printf("%.17e\t%.17e\n", mA, constr.delta_amu());
  }
}
