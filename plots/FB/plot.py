#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.interpolate

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def get_data(filename):
    m, aF, aB = np.genfromtxt(filename, unpack=True, dtype=float)
    return m, aF*1e10, aB*1e10


def plot(datafile, outputfile):
    m, aF, aB = get_data(datafile)
    fig = plt.figure(figsize=(4,4))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='normal')
    plt.xlabel(r"$m_{A}$ / GeV")
    plt.ylabel(r"$a_\mu \times 10^{10}$")
    plt.tight_layout()

    plt.xlim(np.min(m), np.max(m))
    plt.grid(linestyle=':', linewidth=0.5)
    plt.plot(m, aF, 'r-', label=r"$a_\mu^{\text{F}}$")
    plt.plot(m, aB, 'b-.', label=r"$a_\mu^{\text{B}}$")
    legend = plt.legend(loc='upper right', framealpha=1, edgecolor='k')
    legend.set_title("type II")

    plt.savefig(outputfile)
    plt.close()


plot(r'FB.txt', r'FB.pdf')
