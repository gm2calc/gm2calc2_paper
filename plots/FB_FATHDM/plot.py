#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import scipy.interpolate

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def get_data(filename):
    m, aF, aB, a1 = np.genfromtxt(filename, unpack=True, dtype=float)
    return m, aF*1e10, aB*1e10, a1*1e10


def plot(datafile, outputfile):
    m, aF, aB, a1 = get_data(datafile)
    fig = plt.figure(figsize=(4,4))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='normal')
    plt.xlabel(r"$m_{A}$ / GeV")
    plt.ylabel(r"$a_\mu \times 10^{10}$")
    plt.tight_layout()

    plt.xlim(np.min(m), np.max(m))
    plt.grid(linestyle=':', linewidth=0.5)
    plt.plot(m, aF, 'r-', label=r"$a_\mu^{\text{F}}$")
    plt.plot(m, aB, 'b-.', label=r"$a_\mu^{\text{B}}$")
    legend = plt.legend(loc='upper right', framealpha=1, edgecolor='k')
    legend.set_title("flavour aligned")
    
    # draw rectangle
    amean = 25.1    # mean value of a_mu (FNAL+BNL)
    astd = 5.9      # standard deviation of a_mu (FNAL+BNL)
    explanation = 1 # sigma
    # Which points are within 1 sigma of explaining FNAL
    adev = (abs(aF + aB + a1 - amean) / astd) <= explanation

    areg = []
    previous = False
    # Count how many regions can explain FNAL and measure how large they are
    for stp in range(0,len(adev)):
        if previous:
            # Currently inside region that can explain muon g-2
            if adev[stp]:
                # Region continues                
                areg[-1][1] += 1
            else:
                # Region ends
                previous = False
        else:
            # Currently outside region that can explain muon g-2
            if adev[stp]:
                # New region
                areg.append([stp,0])
                previous = True
            else:
                # Do nothing
                pass

    # Width of each mA point on the plot
    width = float(plt.xlim()[1] - plt.xlim()[0]) / len(adev)
    
    for reg in areg:
        x, y = reg[0] + plt.xlim()[0], plt.ylim()[0]
        w, h = reg[1]*width, plt.ylim()[1] - plt.ylim()[0]
        rect = patches.Rectangle((x, y), w, h, linewidth=0, facecolor='purple', alpha=0.3)
        plt.gca().add_patch(rect)

    plt.savefig(outputfile)
    plt.close()


plot(r'FB_FATHDM.txt', r'FB_FATHDM.pdf')
