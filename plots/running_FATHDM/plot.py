#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.interpolate

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def get_data(filename):
    m, an, dan, ar, dar = np.genfromtxt(filename, unpack=True, dtype=float)
    return m, an*1e10, dan*1e10, ar*1e10, dar*1e10


def plot(datafile, outputfile):
    m, an, dan, ar, dar = get_data(datafile)
    fig = plt.figure(figsize=(4,4))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='normal')
    plt.xlabel(r"$m_{A}$ / GeV")
    plt.ylabel(r"$a_\mu^{\text{F}} \times 10^{10}$")
    plt.tight_layout()
    
    plt.xlim(np.min(m), np.max(m))
    plt.grid(linestyle=':', linewidth=0.5)
    plt.plot(m, ar, 'r-', label=r"running masses")
    plt.plot(m, an, 'g--', label=r"input masses")
    plt.fill_between(m, ar + dar, ar - dar, facecolor='r', alpha=0.1)
    plt.fill_between(m, an + dan, an - dan, facecolor='g', alpha=0.1)
    legend = plt.legend(loc='upper right', framealpha=1, edgecolor='k')
    legend.set_title("flavour aligned")
    
    plt.savefig(outputfile)
    plt.close()


plot(r'running_FATHDM.txt', r'running_FATHDM.pdf')
