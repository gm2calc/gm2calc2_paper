#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.interpolate

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def get_grid(filename):
    ms, tb, a2, ax = np.genfromtxt(filename, unpack=True, dtype=float)
    a2 = a2*1e10
    ax = ax*1e10

    N = 200
    msi = np.linspace(ms.min(), ms.max(), N)
    tbi = np.linspace(tb.min(), tb.max(), N)
    a2i = scipy.interpolate.griddata((ms, tb), a2, (msi[None,:], tbi[:,None]), method='cubic')
    axi = scipy.interpolate.griddata((ms, tb), ax, (msi[None,:], tbi[:,None]), method='cubic')

    return msi, tbi, a2i, axi


def plot(xi, yi, zi, outputfile, title):
    fig = plt.figure(figsize=(4,4))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='normal')
    plt.xlabel(r"$m_A$ / GeV")
    plt.ylabel(r"$\tan\beta$")
    plt.xlim(0, np.max(xi))
    plt.ylim(0, np.max(yi))
    plt.grid(linestyle=':', linewidth=0.5)
    plt.tight_layout()

    # draw filled area with correct 1-, 2- and 3-sigma prediction of a_mu
    amu_exp_SM = 25.1
    damu_exp_SM = 5.9
    am3 = plt.contourf(xi, yi, zi, colors=['gray'],
                       levels = [amu_exp_SM - 3*damu_exp_SM, amu_exp_SM + 3*damu_exp_SM])
    am2 = plt.contourf(xi, yi, zi, colors=['yellow'],
                       levels = [amu_exp_SM - 2*damu_exp_SM, amu_exp_SM + 2*damu_exp_SM])
    am1 = plt.contourf(xi, yi, zi, colors=['green'],
                       levels = [amu_exp_SM - damu_exp_SM, amu_exp_SM + damu_exp_SM])

    plt.text(60, 20,title, fontsize = 22)
    plt.savefig(outputfile)
    plt.close()


msi, tbi, a2, ax = get_grid(r'mA-tb.txt')

plot(msi, tbi, a2, r'mA-tb-2.pdf', "type II")
plot(msi, tbi, ax, r'mA-tb-X.pdf', "type X")
