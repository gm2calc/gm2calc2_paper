\documentclass[preprint]{elsarticle}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath,amssymb}
\usepackage{bbm} % \mathbbm{1}
\usepackage[top=3cm,left=3cm,right=3cm,bottom=4cm,footskip=3em]{geometry}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{scalefnt}
\usepackage[usenames]{xcolor}
\usepackage[
  pdftitle={Two-loop Prediction of the Anomalous Magnetic
    Moment of the Muon in the Two-Higgs Doublet Model with GM2Calc 2},
  pdfauthor={
    Peter Athron,
    Csaba Balazs,
    Adriano Cherchiglia,
    Douglas HJ Jacob,
    Dominik Stöckinger,
    Hyejung Stöckinger-Kim,
    Alexander Voigt},
  pdfkeywords={2HDM, anomalous magnetic moment},
  bookmarks=true,
  linktocpage,
  colorlinks=true,
  allbordercolors=white,
  allcolors=fscolor]{hyperref}
\usepackage{orcidlink} % must come AFTER hyperref
\usepackage{textcomp}

\biboptions{sort&compress}

\definecolor{red}{rgb}{0.8,0,0}
\definecolor{green}{rgb}{0,0.5,0}
\definecolor{blue}{rgb}{0,0,0.9}

% source code highlighting
\lstset{breaklines=true,
  breakatwhitespace=true,
  numbers=left,
  stepnumber=1,
  basicstyle=\ttfamily\lst@ifdisplaystyle\footnotesize\fi, % code block in smaller font
  commentstyle=\ttfamily\color{gray},
  prebreak={\textbackslash},
  breakindent=10pt,
  breakautoindent=false,
  showspaces=false,
  showstringspaces=false,
  frame=shadowbox,
  rulesepcolor=\color{gray},
  rulesep=0.1em,
  abovecaptionskip=0em,
  aboveskip=1.5em,
  belowcaptionskip=0.5em,
  belowskip=1em,
  keywordstyle=\color{blue},
  stringstyle=\color{red},
  commentstyle=\color{green},
}

\newcommand{\FS}{\texttt{Flex\-ib\-le\-SU\-SY}}
\newcommand{\FD}{\texttt{Flex\-ib\-le\-De\-cay}}
\newcommand{\GMTCalc}{\texttt{GM2Calc}}
\newcommand{\THDMC}{\texttt{2HDMC}}
\newcommand{\THDECAY}{\texttt{2HDECAY}}
\newcommand{\ScannerS}{\texttt{ScannerS}}
\newcommand{\PROPHECYfourF}{\texttt{PROPHECY4F}}
\newcommand{\SARAH}{\texttt{SARAH}}
\newcommand{\spheno}{\texttt{SPheno}}
\newcommand{\HiggsBounds}{\texttt{HiggsBounds}}
\newcommand{\HiggsSignals}{\texttt{H{\scalefont{.9}IGGS}S{\scalefont{.9}IGNALS}}}
\newcommand{\GAMBIT}{\texttt{GAMBIT}}
\newcommand{\mathematica}{\texttt{Ma\-the\-ma\-ti\-ca}}
\newcommand{\python}{\texttt{Py\-thon}}
\newcommand{\aem}{\ensuremath{\alpha_{\text{em}}}}
\newcommand{\amu}{\ensuremath{a_\mu}}
\newcommand{\amuB}{\ensuremath{\amu^{\text{B}}}}
\newcommand{\amuF}{\ensuremath{\amu^{\text{F}}}}
\newcommand{\amuBSM}{\ensuremath{\amu^{\BSM}}}
\newcommand{\Damu}{\ensuremath{\delta\amu}} % uncertainty
\newcommand{\ol}{\ensuremath{{1\ell}}}
\newcommand{\tl}{\ensuremath{{2\ell}}}
\newcommand{\thl}{\ensuremath{{3\ell}}}
\newcommand{\hc}{\text{h.\,c.}}
\newcommand{\order}{\mathcal{O}}
\DeclareMathOperator{\dilog}{Li_2}
\newcommand{\F}{\mathcal{F}} % loop function
\newcommand{\PiL}{\Pi^0} % (unrotated) Yukawa coupling in the Lagrangian
\newcommand{\rhoL}{\rho^0} % (unrotated) Yukawa coupling in the Lagrangian
\newcommand{\PL}{P_{\scalefont{.9}\text{L}}}
\newcommand{\PR}{P_{\scalefont{.9}\text{R}}}
\newcommand{\real}{\Re\mathfrak{e}}
\newcommand{\VCKM}{V_{\scalefont{.9}\text{CKM}}}
\newcommand{\SM}{{\scalefont{.9}\text{SM}}}
\newcommand{\BSM}{{\scalefont{.9}\text{BSM}}}
\newcommand{\MSSM}{{\scalefont{.9}\text{MSSM}}}
\newcommand{\THDM}{{\scalefont{.9}\text{2HDM}}}
\newcommand{\FATHDM}{{\scalefont{.9}\text{FA2HDM}}}
\newcommand{\MS}{{\overline{\scalefont{.9}\text{MS}}}}
\newcommand{\hSM}{\ensuremath{{h_{\SM}}}}
\newcommand{\SU}{\ensuremath{\text{SU}}}
\newcommand{\id}{\ensuremath{\mathbbm{1}}}
\newcommand{\unit}[1]{\,\text{#1}}
\newcommand{\GeV}{\unit{GeV}}
\DeclareMathOperator{\diag}{diag}
\renewcommand{\imath}{\text{i}}
\newcommand{\appref}[1]{\ref{#1}}
\newcommand{\figref}[1]{\figurename~\ref{#1}}
\newcommand{\secref}[1]{Section~\ref{#1}}
\newcommand{\tabref}[1]{\tablename~\ref{#1}}
\newcommand{\vac}{v}
\newcommand{\Mh}{h}
\newcommand{\MH}{H}
\newcommand{\MA}{A}
\newcommand{\MHpm}{H^{\pm}}
\newcommand{\MHSM}{m_{\Mh_{\SM}}}
\newcommand{\MMh}{m_{\Mh}}
\newcommand{\MMH}{m_{\MH}}
\newcommand{\MMA}{m_{\MA}}
\newcommand{\MMHpm}{m_{\MHpm}}
\newcommand{\avnote}[1]{\textcolor{blue}{[AV: #1]}}
\newcommand{\dhjj}[1]{\textcolor{green}{[DHJJ: #1]}}
\newcommand{\dsnote}[1]{\textcolor{red}{[DS: #1]}}
\newcommand{\ac}[1]{\textcolor{orange}{[AC: #1]}}
\newcommand{\hj}[1]{\textcolor{violet}{[HJ: #1]}}
\newcommand{\pa}[1]{\textcolor{cyan}{[PA: #1]}}
\newcommand{\CsB}[1]{\textcolor{gray}{[CsB: #1]}}


\begin{document}
\begin{frontmatter}
  \title{\Large \bf Two-loop Prediction of the Anomalous Magnetic
    Moment of the Muon in the Two-Higgs Doublet Model with GM2Calc 2}
  \author[nanjing,monash]{Peter Athron\orcidlink{0000-0003-2966-5914}}
  \author[monash]{Csaba Balazs\orcidlink{0000-0001-7154-1726}}
  \author[standre]{Adriano Cherchiglia\orcidlink{0000-0002-2457-1671}}
  \author[monash]{Douglas Jacob\orcidlink{0000-0002-8950-2853}\corref{cor1}}
  \ead{douglas.jacob@monash.edu}
  \cortext[cor1]{Corresponding author}
  \author[dresden]{Dominik Stöckinger}
  \author[dresden]{Hyejung Stöckinger-Kim}
  \author[flensburg]{Alexander Voigt\orcidlink{0000-0001-8963-6512}}
  \address[nanjing]{Department  of  Physics  and  Institute  of  Theoretical  Physics,  Nanjing  Normal  University, Nanjing, Jiangsu 210023, China}
  \address[monash]{ARC Centre of Excellence for Particle Physics at
    the Terascale, School of Physics, Monash University, Melbourne,
    Victoria 3800, Australia}
  \address[standre]{Centro de Ci\^ecias Naturais e Humanas, Universidade Federal do ABC, Santo Andr\'e, Brazil}
  \address[dresden]{Institut für Kern- und Teilchenphysik,
    TU Dresden, Zellescher Weg 19, 01069 Dresden, Germany}
  \address[flensburg]{Fachbereich Energie und Biotechnologie, Hochschule Flensburg, Kanzleistraße 91--93, 24943 Flensburg, Germany}

  \input{tex/abstract}

  \begin{keyword}
    Muon anomalous magnetic moment,
    Two-Higgs Doublet Model
  \end{keyword}
\end{frontmatter}

\clearpage
\tableofcontents
\clearpage

\input{tex/introduction}
\input{tex/thdmamu}
\input{tex/program}
\input{tex/applications}
\input{tex/conclusion}
\input{tex/acknowledgments}

\addcontentsline{toc}{section}{References}

\bibliography{paper,TheoryWPbiblio,MuonMoment2HDM}
\bibliographystyle{JHEP}
\end{document}
