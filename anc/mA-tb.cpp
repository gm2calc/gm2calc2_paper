#include "gm2calc/gm2_1loop.hpp"
#include "gm2calc/gm2_2loop.hpp"
#include "gm2calc/gm2_error.hpp"
#include "gm2calc/THDM.hpp"

#include <cstdio>
#include <limits>

// Calculates amu in the mA-tan(beta) plane as in Fig.3 from
// arxiv:1409.3199.
double calc_amu(double mA, double tb, gm2calc::thdm::Yukawa_type yukawa_type)
{
   gm2calc::SM sm;
   const double v = sm.get_v();
   const double mh = 126, mH = 200;
   const double lambda_max = 3.5449077018110321; // Sqrt[4 Pi]
   const double lambda1 = lambda_max;

   gm2calc::thdm::Mass_basis basis;
   basis.yukawa_type = yukawa_type;
   basis.mh = mh;
   basis.mH = mH;
   basis.mA = mA;
   basis.mHp = mH;
   basis.sin_beta_minus_alpha = 1;
   basis.lambda_6 = 0;
   basis.lambda_7 = 0;
   basis.tan_beta = tb;
   basis.m122 = mH*mH/tb + (mh*mh - lambda1*v*v)/(tb*tb*tb); // Eq.(14)

   double amu = std::numeric_limits<double>::quiet_NaN();

   try {
      gm2calc::THDM model(basis, sm);

      amu = gm2calc::calculate_amu_1loop(model)
          + gm2calc::calculate_amu_2loop(model);
   } catch (const gm2calc::Error&) {}

   return amu;
}

int main()
{
   const double mA_start = 1, mA_stop = 100;
   const double tb_start = 1, tb_stop = 100;
   const int N_steps = 198;

   std::printf("%-20s%-20s%-20s%-20s\n",
               "# mA/GeV", "tan(beta)", "amu(II)", "amu(X)");

   for (int i = 0; i <= N_steps; i++) {
      for (int k = 0; k <= N_steps; k++) {
         const double tb = tb_start + i*(tb_stop - tb_start)/N_steps;
         const double mA = mA_start + k*(mA_stop - mA_start)/N_steps;

         const auto type_2 = calc_amu(mA, tb, gm2calc::thdm::Yukawa_type::type_2);
         const auto type_X = calc_amu(mA, tb, gm2calc::thdm::Yukawa_type::type_X);

         std::printf("%-20.10e%-20.10e%-20.10e%-20.10e\n",
                     mA, tb, type_2, type_X);
      }
   }

   return 0;
}
