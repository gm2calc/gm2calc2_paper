#include "gm2calc/gm2_1loop.h"
#include "gm2calc/gm2_2loop.h"
#include "gm2calc/gm2_uncertainty.h"
#include "gm2calc/THDM.h"
#include "gm2calc/SM.h"

#include <stdio.h>

int main()
{
   /* define THDM parameters in the mass basis */
   const gm2calc_THDM_mass_basis basis = {
      .yukawa_type = gm2calc_THDM_type_2,       /* Yukawa type */
      .mh = 125,                                /* light CP-even Higgs mass */
      .mH = 400,                                /* heavy CP-even Higgs mass */
      .mA = 420,                                /* CP-odd Higgs mass */
      .mHp = 440,                               /* charged Higgs mass */
      .sin_beta_minus_alpha = 0.999,            /* sin(beta - alpha) */
      .lambda_6 = 0,                            /* lambda_6 */
      .lambda_7 = 0,                            /* lambda_7 */
      .tan_beta = 3,                            /* tan(beta) */
      .m122 = 40000,                            /* m_{12}^2 in GeV^2 */
      .zeta_u = 0,                              /* zeta_u */
      .zeta_d = 0,                              /* zeta_d */
      .zeta_l = 0,                              /* zeta_l */
      .Delta_u = { {0,0,0}, {0,0,0}, {0,0,0} }, /* Re(Delta_u(i,k)) */
      .Delta_d = { {0,0,0}, {0,0,0}, {0,0,0} }, /* Re(Delta_d(i,k)) */
      .Delta_l = { {0,0,0}, {0,0,0}, {0,0,0} }, /* Re(Delta_l(i,k)) */
      .Pi_u = { {0,0,0}, {0,0,0}, {0,0,0} },    /* Re(Pi_u(i,k)) */
      .Pi_d = { {0,0,0}, {0,0,0}, {0,0,0} },    /* Re(Pi_d(i,k)) */
      .Pi_l = { {0,0,0}, {0,0,0}, {0,0,0} }     /* Re(Pi_l(i,k)) */
   };

   /* define SM parameters */
   gm2calc_SM sm;
   gm2calc_sm_set_to_default(&sm);
   sm.alpha_em_mz = 1.0/128.94579;  /* electromagnetic coupling */
   sm.mu[2] = 173.34;               /* top quark mass */
   sm.mu[1] = 1.28;                 /* charm quark mass */
   sm.md[2] = 4.18;                 /* bottom quark mass */
   sm.ml[2] = 1.77684;              /* tau lepton mass */

   /* calculation settings */
   gm2calc_THDM_config config;
   gm2calc_thdm_config_set_to_default(&config);

   /* setup the THDM */
   gm2calc_THDM* model = 0;
   gm2calc_error error = gm2calc_thdm_new_with_mass_basis(&model, &basis, &sm, &config);

   if (error == gm2calc_NoError) {
      /* calculate a_mu up to (including) the 2-loop level */
      const double amu = gm2calc_thdm_calculate_amu_1loop(model)
                       + gm2calc_thdm_calculate_amu_2loop(model);

      /* calculate the uncertainty of the 2-loop a_mu */
      const double delta_amu =
         gm2calc_thdm_calculate_uncertainty_amu_2loop(model);

      printf("amu = %g +- %g\n", amu, delta_amu);
   } else {
      printf("Error: %s\n", gm2calc_error_str(error));
   }

   gm2calc_thdm_free(model);

   return 0;
}
