Install["bin/gm2calc.mx"];

CalcAmu[mA_] :=
    {amu2LF, Damu} /. GM2CalcAmuTHDMMassBasis[
        yukawaType        -> 2,
        Mhh               -> { 125, 400 },
        MAh               -> mA,
        MHp               -> 440,
        sinBetaMinusAlpha -> 0.999,
        lambda6           -> 0,
        lambda7           -> 0,
        TB                -> 3,
        m122              -> 200^2
    ];

(* mA values in [130,500] GeV *)
mAValues = Subdivide[130, 500, 200];

(* calculation w/o running couplings *)
GM2CalcSetFlags[runningCouplings -> False];
noRunning = CalcAmu /@ mAValues;

(* calculation w/ running couplings *)
GM2CalcSetFlags[runningCouplings -> True];
withRunning = CalcAmu /@ mAValues;

data = { mAValues,
         noRunning[[All,1]], noRunning[[All,2]],
         withRunning[[All,1]], withRunning[[All,2]] };

Export["running.txt", N @ Transpose @ data, "Table"];
