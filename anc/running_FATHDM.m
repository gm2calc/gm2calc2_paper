(* ::Package:: *)

Install["bin/gm2calc.mx"];

CalcAmu[mA_] :=
    {amu2LF, Damu} /. GM2CalcAmuTHDMMassBasis[
        yukawaType        -> 5,
        Mhh               -> { 125, 150 },
        MAh               -> mA,
        MHp               -> 150,
        sinBetaMinusAlpha -> 0.999,
        lambda6           -> 0,
        lambda7           -> 0,
        TB                -> 2.,
        m122              -> 3187.3 + 3.27803*mA + 0.0165557*mA^2,
        zetau             -> -0.1,
        zetad             -> -0.1,
        zetal             -> 50
    ];

(* mA values in [20,60] GeV *)
mAValues = Subdivide[20, 60, 200];

(* calculation w/o running couplings *)
GM2CalcSetFlags[runningCouplings -> False];
noRunning = CalcAmu /@ mAValues;

(* calculation w/ running couplings *)
GM2CalcSetFlags[runningCouplings -> True];
withRunning = CalcAmu /@ mAValues;

data = { mAValues,
         noRunning[[All,1]], noRunning[[All,2]],
         withRunning[[All,1]], withRunning[[All,2]] };

Export["running_FATHDM.txt", N @ Transpose @ data, "Table"];
