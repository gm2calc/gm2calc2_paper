Install["bin/gm2calc.mx"];

(* calculation settings *)
GM2CalcSetFlags[
    loopOrder -> 2,
    runningCouplings -> True];

(* set SM parameters *)
GM2CalcSetSMParameters[
    alpha0 -> 0.00729735,      (* alpha_em in Thompson limit *)
    alphaMZ -> 0.0077552,      (* alpha_em(MZ) *)
    alphaS -> 0.1184,          (* alpha_s *)
    MhSM -> 125.09,            (* SM Higgs boson pole mass *)
    MW -> 80.385,              (* W boson pole mass *)
    MZ -> 91.1876,             (* Z boson pole mass *)
    MT -> 173.34,              (* top quark pole mass *)
    mcmc -> 1.28,              (* charm quark MS-bar mass mc at Q = mc *)
    mu2GeV -> 0.0022,          (* up quark MS-bar mass at Q = 2 GeV *)
    mbmb -> 4.18,              (* bottom quark MS-bar mass mb at Q = mb *)
    ms2GeV -> 0.096,           (* strange quark MS-bar mass at Q = 2 GeV *)
    md2GeV -> 0.0047,          (* down quark MS-bar mass at Q = 2 GeV *)
    ML -> 1.777,               (* tau lepton pole mass *)
    MM -> 0.1056583715,        (* muon pole mass *)
    ME -> 0.000510998928,      (* electron pole mass *)
    Mv1 -> 0,                  (* lightest neutrino mass *)
    Mv2 -> 0,                  (* 2nd lightest neutrino mass *)
    Mv3 -> 0,                  (* heaviest neutrino mass *)
    CKM -> IdentityMatrix[3]   (* CKM matrix *)
];

(* calculate amu using the gauge basis input parameters *)
result = {amu, Damu} /. GM2CalcAmuTHDMGaugeBasis[
    yukawaType        -> 2,                    (* Yukawa type (1,...,6) *)
    lambda            -> { 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1 }, (* lambda_{1,...,7} *)
    TB                -> 3,                    (* tan(beta) = v2/v1 *)
    m122              -> 200^2,                (* m_{12}^2 *)
    zetau             -> 0,                    (* zeta_u *)
    zetad             -> 0,                    (* zeta_d *)
    zetal             -> 0,                    (* zeta_l *)
    Deltau            -> 0 IdentityMatrix[3],  (* Delta_u *)
    Deltad            -> 0 IdentityMatrix[3],  (* Delta_d *)
    Deltal            -> 0 IdentityMatrix[3],  (* Delta_l *)
    Piu               -> 0 IdentityMatrix[3],  (* Pi_u *)
    Pid               -> 0 IdentityMatrix[3],  (* Pi_d *)
    Pil               -> 0 IdentityMatrix[3]   (* Pi_l *)
  ];

Print[result];
