#!/usr/bin/env python

from __future__ import print_function
from gm2_python_interface import *

cppyy.include(os.path.join("gm2calc","gm2_1loop.hpp"))
cppyy.include(os.path.join("gm2calc","gm2_2loop.hpp"))
cppyy.include(os.path.join("gm2calc","gm2_uncertainty.hpp"))
cppyy.include(os.path.join("gm2calc","gm2_error.hpp"))
cppyy.include(os.path.join("gm2calc","SM.hpp"))
cppyy.include(os.path.join("gm2calc","THDM.hpp"))

cppyy.load_library("libgm2calc")

# Load data types
from cppyy.gbl import Eigen
from cppyy.gbl import gm2calc
from cppyy.gbl.gm2calc import SM
from cppyy.gbl.gm2calc import THDM
from cppyy.gbl.gm2calc import Error

# define THDM parameters in the mass basis
basis = gm2calc.thdm.Mass_basis()
basis.yukawa_type = gm2calc.thdm.Yukawa_type.type_2
basis.mh = 125.                            # light CP-even Higgs mass
basis.mH = 400.                            # heavy CP-even Higgs mass
basis.mA = 420.                            # CP-odd Higgs mass
basis.mHp = 440.                           # charged Higgs mass
basis.sin_beta_minus_alpha = 0.999         # sin(beta - alpha)
basis.lambda_6 = 0.                        # lambda_6
basis.lambda_7 = 0.                        # lambda_7
basis.tan_beta = 3.                        # tan(beta)
basis.m122 = 40000.                        # m_{12}^2 in GeV^2
basis.zeta_u = 0.                          # zeta_u
basis.zeta_d = 0.                          # zeta_d
basis.zeta_l = 0.                          # zeta_l
basis.Delta_u = Eigen.Matrix3d().setZero() # Delta_u
basis.Delta_d = Eigen.Matrix3d().setZero() # Delta_d
basis.Delta_l = Eigen.Matrix3d().setZero() # Delta_l
basis.Pi_u = Eigen.Matrix3d().setZero()    # Pi_u
basis.Pi_d = Eigen.Matrix3d().setZero()    # Pi_d
basis.Pi_l = Eigen.Matrix3d().setZero()    # Pi_l
# define SM parameters
sm = gm2calc.SM()
sm.set_alpha_em_mz(1.0/128.94579)  # electromagnetic coupling
sm.set_mu(2, 173.34)               # top quark mass
sm.set_mu(1, 1.28)                 # charm quark mass
sm.set_md(2, 4.18)                 # bottom quark mass
sm.set_ml(2, 1.77684)              # tau lepton mass
# define options to customize the calculation
config = gm2calc.thdm.Config()
config.running_couplings = True;   # use running couplings

try:
    # setup the THDM
    model = gm2calc.THDM(basis,sm,config)
    # calculate a_mu up to (including) the 2-loop level
    amu = gm2calc.calculate_amu_1loop(model) + gm2calc.calculate_amu_2loop(model)
    # calculate the uncertainty of the 2-loop a_mu
    delta_amu = gm2calc.calculate_uncertainty_amu_2loop(model)
    print("amu =",amu,"+-",delta_amu)
except gm2calc.Error as e:
    print(e.what())

