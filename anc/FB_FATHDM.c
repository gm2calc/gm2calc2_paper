#include "gm2calc/gm2_1loop.h"
#include "gm2calc/gm2_2loop.h"
#include "gm2calc/gm2_error.h"
#include "gm2calc/THDM.h"

#include <stdio.h>
#include <math.h>

int main()
{
   const double mA_start = 20, mA_stop = 60;
   const int N_steps = 200;

   printf("%-20s%-20s%-20s%-20s\n", "# mS/GeV", "amu(F)", "amu(B)", "amu(1)");

   for (int i = 0; i <= N_steps; i++) {
      const double mA = mA_start + i*(mA_stop - mA_start)/N_steps;

      const gm2calc_THDM_mass_basis basis = {
         .yukawa_type = gm2calc_THDM_aligned,
         .mh = 125,
         .mH = 150,
         .mA = mA,
         .mHp = 150,
         .sin_beta_minus_alpha = 0.999,
         .lambda_6 = 0,
         .lambda_7 = 0,
         .tan_beta = 2,
         // Polynomial fit following Eq. (2.12) from PRD
         .m122 = 3187.3 + mA*(3.27803 + 0.0165557*mA),
         .zeta_u = -0.1,
         .zeta_d = -0.1,
         .zeta_l = 50,
         .Delta_u = { {0,0,0}, {0,0,0}, {0,0,0} },
         .Delta_d = { {0,0,0}, {0,0,0}, {0,0,0} },
         .Delta_l = { {0,0,0}, {0,0,0}, {0,0,0} },
         .Pi_u = { {0,0,0}, {0,0,0}, {0,0,0} },
         .Pi_d = { {0,0,0}, {0,0,0}, {0,0,0} },
         .Pi_l = { {0,0,0}, {0,0,0}, {0,0,0} }
      };

      gm2calc_THDM* model = 0;
      gm2calc_error error = gm2calc_thdm_new_with_mass_basis(&model, &basis, 0, 0);

      if (error == gm2calc_NoError) {
         const double amu1 = gm2calc_thdm_calculate_amu_1loop(model);
         const double amuF = gm2calc_thdm_calculate_amu_2loop_fermionic(model);
         const double amuB = gm2calc_thdm_calculate_amu_2loop_bosonic(model);

         printf("%-20.10e%-20.10e%-20.10e%-20.10e\n", basis.mA, amuF, amuB, amu1);
      } else {
         printf("Error: %s\n", gm2calc_error_str(error));
      }

      gm2calc_thdm_free(model);
   }

   return 0;
}
