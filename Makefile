NAME := paper
PDF  := $(NAME).pdf
BBL  := $(NAME).bbl
BIB  := $(NAME).bib MuonMoment2HDM.bib TheoryWPbiblio.bib
IMG  := img/FB.pdf \
	img/FB_FATHDM.pdf \
	img/mA-tb-2.pdf \
	img/mA-tb-X.pdf \
	img/running.pdf \
	img/running_FATHDM.pdf
SRC  := anc/example_gauge-basis.c \
	anc/example_gauge-basis.cpp \
	anc/example_gauge-basis.m \
	anc/example_gauge_basis.py \
	anc/example_mass-basis.c \
	anc/example_mass-basis.cpp \
	anc/example_mass-basis.m \
	anc/example_mass_basis.py \
	anc/FB.c \
	anc/FB_FATHDM.c \
	anc/mA-tb.cpp \
	anc/running.m \
	anc/running_FATHDM.m
STY  := JHEP.bst orcidlink.sty
TAR  := $(NAME).tar.gz
TEX  := $(NAME).tex \
	tex/abstract.tex \
	tex/acknowledgments.tex \
	tex/thdmamu.tex \
	tex/applications.tex \
	tex/conclusion.tex \
	tex/introduction.tex \
	tex/program.tex

CPP_SRC := \
	anc/example_gauge-basis.cpp \
	anc/example_mass-basis.cpp \
	anc/mA-tb.cpp
CPP_EXE := $(CPP_SRC:.cpp=.x)
C_SRC   := anc/example_gauge-basis.c \
	   anc/example_mass-basis.c \
	   anc/FB.c \
	   anc/FB_FATHDM.c
C_EXE   := $(C_SRC:.c=.cx)

EIGEN_DIR ?= /usr/include/eigen3/

all: $(PDF)

clean: clean-exe
	-rm -f *.aux *.bbl *.blg *.log *.out *.spl *.toc

clean-exe:
	-rm -f $(CPP_EXE) $(C_EXE)

distclean: clean
	-rm -f $(PDF)

$(PDF): $(TEX) $(BIB) $(IMG) $(SRC)
	pdflatex $<
	bibtex $(NAME)
	pdflatex $<
	pdflatex $<

$(TAR): $(TEX) $(BIB) $(BBL) $(IMG) $(SRC) $(STY) | $(PDF)
	tar --transform 's,^,$(NAME)/,' -czf $@ $^

arxiv: $(TAR)
	@true

arxiv-test: $(TAR)
	tar -xf $(TAR)
	cd $(NAME) && pdflatex $(NAME).tex && pdflatex $(NAME).tex
	rm -r $(NAME) $(TAR)

img/FB.pdf:
	cd plots/FB/ && ./plot.py
	mv plots/FB/$(notdir $@) $@

img/FB_FATHDM.pdf:
	cd plots/FB_FATHDM/ && ./plot.py
	mv plots/FB_FATHDM/$(notdir $@) $@

img/mA-tb-2.pdf img/mA-tb-X.pdf:
	cd plots/mA-tb/ && ./plot.py
	mv plots/mA-tb/$(notdir $@) $(dir $@)

img/running.pdf:
	cd plots/running/ && ./plot.py
	mv plots/running/$(notdir $@) $@

img/running_FATHDM.pdf:
	cd plots/running_FATHDM/ && ./plot.py
	mv plots/running_FATHDM/$(notdir $@) $@

compile: $(CPP_EXE) $(C_EXE)

%.x: %.cpp
	$(CXX) -Wall -Wextra -O2 -I$(GM2CALC_DIR)/include/ -I$(EIGEN_DIR) -o $@ $< $(GM2CALC_DIR)/build/lib/libgm2calc.so

%.cx: %.c
	$(CC) -Wall -Wextra -O2 -I$(GM2CALC_DIR)/include/ -o $@ $< $(GM2CALC_DIR)/build/lib/libgm2calc.so
