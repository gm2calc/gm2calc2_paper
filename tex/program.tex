\section{Program details and usage} 
\label{sec:implementation}


\subsection{Quick start}

\GMTCalc\ can be downloaded from \url{https://gm2calc.hepforge.org/}
or \url{https://github.com/GM2Calc/GM2Calc}, for example as%
\footnote{To track changes made to \GMTCalc\ and get automatic
  updates one may alternatively clone the \lstinline|git| repository
  \url{https://github.com/GM2Calc/GM2Calc}}
%
\begin{lstlisting}[language=bash]
wget --content-disposition https://github.com/GM2Calc/GM2Calc/archive/v2.0.0.tar.gz
tar -xf GM2Calc-2.0.0.tar.gz
cd GM2Calc-2.0.0
\end{lstlisting}
%
To build \GMTCalc\ run the following commands:
%
\begin{lstlisting}[language=bash]
mkdir build
cd build
cmake ..
make
\end{lstlisting}
%
To calculate $\amuBSM$ in the \THDM\ with \GMTCalc\ from the command line,
run
%
\begin{lstlisting}[language=bash]
bin/gm2calc.x --thdm-input-file=../input/example.thdm
\end{lstlisting}
%
Here, \lstinline|example.thdm| is the name of the file that contains
the input parameters.\footnote{For instructions and examples of
  running the \MSSM\ evaluation see Ref.\ \cite{Athron:2015rva}.}


\subsection{Requirements}

To build \GMTCalc\ the following programs and libraries are required:
%
\begin{itemize}
\item C++14 and C11 compatible compilers
\item Eigen library, version 3.1 or higher \cite{eigenweb}
  [\url{http://eigen.tuxfamily.org}]
\item Boost library, version 1.37.0 or higher \cite{BoostLibrary}
  [\url{http://www.boost.org}]
\item (optional) \lstinline|Wolfram| \mathematica\ or \lstinline|Wolfram Engine|
  \cite{Mathematica} 
\item (optional) \python\ 2 or \python\ 3 \cite{Python}, using the package
  \lstinline|cppyy| \cite{cppyy}
  [\url{https://pypi.org/project/cppyy/}] 
\end{itemize}


\subsection{Running \GMTCalc\ from the command line}

\GMTCalc\ can be run from the command line with an SLHA-like
\cite{Skands:2003cj,Allanach:2008qq} input file as
%
\begin{lstlisting}[language=bash]
bin/gm2calc.x --thdm-input-file=example.thdm
\end{lstlisting}
%
where \lstinline|example.thdm| is the name of the SLHA-like input file.
Alternatively, the input parameters can be piped in an SLHA-like
format into \GMTCalc\ as
%
\begin{lstlisting}[language=bash]
cat example.thdm | bin/gm2calc.x --thdm-input-file=-
\end{lstlisting}
%
The calculated value for $\amuBSM$ and $\Damu$ is written to the standard
output.  Depending on the input options and parameters, the output may
contain the following block with $\amuBSM$ and $\Damu$:
%
\begin{lstlisting}[language=bash]
Block GM2CalcOutput
     0     1.67323025E-11   # Delta(g-2)_muon/2
     1     3.36159655E-12   # uncertainty of Delta(g-2)_muon/2
\end{lstlisting}
%
The SLHA-like format for the input parameters is defined in the
following subsections.


\subsubsection{General options}

In the SLHA-like input the selection of the configuration flags for
\GMTCalc\ can be given in the \lstinline|GM2CalcConfig| block as
defined in Ref.\ \cite{Athron:2015rva}.  An example
\lstinline|GM2CalcConfig| block reads as follows:
%
\begin{lstlisting}
Block GM2CalcConfig
     0     4     # output format (0 = minimal, 1 = detailed,
                 #  2 = NMSSMTools, 3 = SPheno, 4 = GM2Calc)
     1     2     # loop order (0, 1 or 2)
     2     1     # disable/enable tan(beta) resummation (0 or 1)
     3     0     # force output (0 or 1)
     4     0     # verbose output (0 or 1)
     5     1     # calculate uncertainty (0 or 1)
     6     1     # running couplings in the THDM
\end{lstlisting}
%
The entry \lstinline|GM2CalcConfig[0]| defines the output format.  If
\lstinline|GM2CalcConfig[0] = 0|, a single number is printed to the
\lstinline|stdout|.  This number is the value of $\amuBSM$ or the
uncertainty $\Damu$, depending on the value of
\lstinline|GM2CalcConfig[5]|: If \lstinline|GM2CalcConfig[5] = 0|, the
value of $\amuBSM$ is printed.  If \lstinline|GM2CalcConfig[5] = 1|, the
value of $\Damu$ is printed.  If \lstinline|GM2CalcConfig[0] = 1|,
a detailed output, suitable for debugging, is printed.  If
\lstinline|GM2CalcConfig[0] = 2|, the value of $\amuBSM$ is written to the
output block entry \lstinline|LOWEN[6]|.  If
\lstinline|GM2CalcConfig[0] = 3|, the value of $\amuBSM$ is written to the
output block entry \lstinline|SPhenoLowEnergy[21]|.  If
\lstinline|GM2CalcConfig[0] = 4| (default), the value of $\amuBSM$ is
written to the output block entry \lstinline|GM2CalcOutput[0]|.

The entry \lstinline|GM2CalcConfig[1]| defines the loop order of the
calculation, which can be set to \lstinline|0|, \lstinline|1| or
\lstinline|2|.  The default value is \lstinline|2| (recommended),
which corresponds to the two-loop calculation of $\amuBSM$.

The entry \lstinline|GM2CalcConfig[2]| disables/enables the
resummation of $\tan\beta$ in the \MSSM.  By default $\tan\beta$
resummation is enabled, which corresponds to
\lstinline|GM2CalcConfig[2] = 1|.  To disable $\tan\beta$ resummation,
set \lstinline|GM2CalcConfig[2] = 0|.  When the calculation is
performed in the \THDM, the value of \lstinline|GM2CalcConfig[2]| is
ignored.

The next two options are useful for debugging.  
By setting the entry \lstinline|GM2CalcConfig[3] = 1| (default:
\lstinline|0|), the output of \GMTCalc\ can be forced, even if a
physical problem (e.g.\ a tachyon) has occurred.  Warning: If a
physical problem has occurred, the output cannot be trusted.  Forcing
the output should only be used for debugging.
By setting the entry \lstinline|GM2CalcConfig[4] = 1| (default:
\lstinline|0|), additional information about the model parameters and
the calculation is printed.  

With the entry \lstinline|GM2CalcConfig[5]| the calculation of the
uncertainty $\Damu$, c.f.\ Eq.~\eqref{eq:damu}, can be
disabled/enabled (\lstinline|0| or \lstinline|1|).

The entry \lstinline|GM2CalcConfig[6]| (default: \lstinline|1|) is new
in \GMTCalc\ 2.0.0 and controls the definition of the fermion masses
that are inserted into the fermionic two-loop contribution $\amuF$,
see \secref{sec:running_couplings}.  If \lstinline|GM2CalcConfig[6] = 0|,
the input masses \eqref{eq:input_masses} are used.  If
\lstinline|GM2CalcConfig[6] = 1|, the running masses
\eqref{eq:running_masses} are used.  The difference between these two
schemes is shown in \secref{sec:applications}.


\subsubsection{Standard Model input parameters}

The Standard Model input parameters are read from the
\lstinline|SMINPUTS|, \lstinline|GM2CalcInput| and \lstinline|VCKMIN|
blocks.  Example blocks that define the Standard Model input
parameters may read:
%
\begin{lstlisting}
Block SMINPUTS
     1     128.94579        # alpha_em(MZ)^(-1) SM MS-bar
     3     0.1184           # alpha_s(MZ) SM MS-bar
     4     91.1876          # MZ(pole)
     5     4.18             # mb(mb) SM MS-bar
     6     173.34           # mtop(pole)
     7     1.77684          # mtau(pole)
     8     0                # mnu3(pole)
     9     80.385           # mW(pole)
    11     0.000510998928   # melectron(pole)
    12     0                # mnu1(pole)
    13     0.1056583715     # mmuon(pole)
    14     0                # mnu2(pole)
    21     0.0047           # md(2 GeV)
    22     0.0022           # mu(2 GeV)
    23     0.096            # ms(2 GeV)
    24     1.28             # mc(2 GeV)
Block GM2CalcInput
    33     125.09           # SM Higgs boson mass
Block VCKMIN                # CKM matrix in Wolfenstein parametrization
     1     0.2257           # lambda
     2     0.814            # A
     3     0.135            # rho-bar
     4     0.349            # eta-bar
\end{lstlisting}
%
The entries of the \lstinline|SMINPUTS| and \lstinline|VCKMIN| are
defined in Refs.\ \cite{Skands:2003cj,Allanach:2008qq}.  In the block
entry \lstinline|GM2CalcInput[33]| the mass of the Standard Model
Higgs boson can be given.  Unset parameters in the
\lstinline|SMINPUTS| and \lstinline|GM2CalcInput| blocks are
assigned default values.  Unset parameters in the \lstinline|VCKMIN|
block are assumed to be zero.


\subsubsection{Two-Higgs Doublet Model input parameters}

We allow the specification of the \THDM\ input parameters in two
different ``bases'', see \tabref{tab:basis}.  The basis parameters are
defined as in \cite{Eriksson:2009ws}.  In the ``gauge basis'' the
Lagrangian parameters, $\lambda_{1, \ldots, 7}$
are used as input.  In the ``mass basis'' the
Higgs boson masses and the mixing parameter $\sin(\beta-\alpha)$ are
used as input, instead of the Lagrangian parameters $\lambda_{1, \ldots, 5}$,
but $\lambda_6$ and $\lambda_7$ can still be used.
All available input parameters are listed in
\tabref{tab:basis2}.
%
\begin{table}[tb]
  \centering
  \caption{\THDM\ input parameters for different basis ($f=u,d,l$).}
  \begin{tabular}{ll}
    \toprule
    basis    & input parameters \\
    \midrule
    gauge  & $\lambda_1$, \ldots, $\lambda_7$, $\tan\beta$, $m_{12}^2$, $\zeta_f$, $\Delta_f$, $\Pi_f$ \\
    mass   & $m_h$, $m_H$, $m_A$, $m_{H^\pm}$, $\sin(\beta-\alpha)$, $\lambda_6$, $\lambda_7$, $\tan\beta$, $m_{12}^2$, $\zeta_f$, $\Delta_f$, $\Pi_f$ \\
    \bottomrule
  \end{tabular}
  \label{tab:basis}
\end{table}


\paragraph{Mass basis input parameters}

\begin{table}[tb]
  \centering
  \caption{\THDM\ input parameters for the SLHA and \mathematica\ interface.}
  \begin{tabular}{llll}
    \toprule
    input parameter & SLHA entry & \mathematica\ symbol & allowed values \\
    \midrule
    \multicolumn{4}{c}{parameters specific to the gauge basis} \\
    \midrule
    $\lambda_1,\ldots,\lambda_7$ & \lstinline|MINPAR[11]|, \ldots, \lstinline|MINPAR[17]| & \lstinline|lambda| & $\mathbb{R}$ \\
    \midrule
    \multicolumn{4}{c}{parameters specific to the mass basis} \\
    \midrule
    $\lambda_6$ & \lstinline|MINPAR[16]| & \lstinline|lambda6| & $\mathbb{R}$ \\
    $\lambda_7$ & \lstinline|MINPAR[17]| & \lstinline|lambda7| & $\mathbb{R}$ \\
    $\sin(\beta-\alpha)$ & \lstinline|MINPAR[20]| & \lstinline|sinBetaMinusAlpha| & $[-1,1]$ \\
    $\{m_h,m_H\}$ & \lstinline|MASS[25]|, \lstinline|MASS[35]| & \lstinline|Mhh| & $\{\mathbb{R}_{\geq 0},\mathbb{R}_{\geq 0}\}$ \\
    $m_A$ & \lstinline|MASS[36]| & \lstinline|MAh| & $\mathbb{R}_{\geq 0}$ \\
    $m_{H^\pm}$ & \lstinline|MASS[37]| & \lstinline|MHp| & $\mathbb{R}_{\geq 0}$ \\
    \midrule
    \multicolumn{4}{c}{parameters common to both the gauge and mass basis} \\
    \midrule
    Yukawa type & \lstinline|MINPAR[24]| & \lstinline|yukawaType| & $1$, \ldots, $6$ \\
    $\tan\beta$ & \lstinline|MINPAR[3]| & \lstinline|TB| & $\mathbb{R}_{>0}$ \\
    $m_{12}^2$ & \lstinline|MINPAR[18]| & \lstinline|m122| & $\mathbb{R}$ \\
    $\zeta_u$ & \lstinline|MINPAR[21]| & \lstinline|zetau| & $\mathbb{R}$ \\
    $\zeta_d$ & \lstinline|MINPAR[22]| & \lstinline|zetad| & $\mathbb{R}$ \\
    $\zeta_l$ & \lstinline|MINPAR[23]| & \lstinline|zetal| & $\mathbb{R}$ \\
    $\Delta_u$ & \lstinline|GM2CalcTHDMDeltauInput| & \lstinline|Deltau| & $\mathbb{R}^{3\times 3}$ \\
    $\Delta_d$ & \lstinline|GM2CalcTHDMDeltadInput| & \lstinline|Deltad| & $\mathbb{R}^{3\times 3}$ \\
    $\Delta_l$ & \lstinline|GM2CalcTHDMDeltalInput| & \lstinline|Deltal| & $\mathbb{R}^{3\times 3}$ \\
    $\Pi_u$ & \lstinline|GM2CalcTHDMPiuInput| & \lstinline|Piu| & $\mathbb{R}^{3\times 3}$ \\
    $\Pi_d$ & \lstinline|GM2CalcTHDMPidInput| & \lstinline|Pid| & $\mathbb{R}^{3\times 3}$ \\
    $\Pi_l$ & \lstinline|GM2CalcTHDMPilInput| & \lstinline|Pil| & $\mathbb{R}^{3\times 3}$ \\
    \bottomrule
  \end{tabular}
  \label{tab:basis2}
\end{table}
%
The ``mass basis'' input parameters are read from the
\lstinline|MINPAR| and \lstinline|MASS| blocks for compatibility with
\THDMC\ \cite{Eriksson:2009ws}.  The following shows an example input
in the ``mass basis'' for the type II \THDM:
%
\begin{lstlisting}
Block MINPAR                  # model parameters
    3        3                # tan(beta)
   16        0                # lambda_6
   17        0                # lambda_7
   18        40000            # m_{12}^2
   20        0.999            # sin(beta - alpha)
   21        0                # zeta_u (only used if Yukawa type = 5)
   22        0                # zeta_d (only used if Yukawa type = 5)
   23        0                # zeta_l (only used if Yukawa type = 5)
   24        2                # Yukawa type (1, 2, 3, 4, 5 = aligned, 6 = general)
Block MASS                    # Higgs masses
   25        125              # mh, lightest CP-even Higgs
   35        400              # mH, heaviest CP-even Higgs
   36        420              # mA, CP-odd Higgs
   37        440              # mH+, charged Higgs
\end{lstlisting}
%
Unset parameters in the \lstinline|MINPAR| and \lstinline|MASS| blocks
are assumed to be zero, except for $\tan\beta$ which will raise an
error. Entry 24 of the \lstinline|MINPAR| is used to select the type
of \THDM. Specifically the integer values $1$, \ldots, $6$ for the
Yukawa type correspond to: $1$ = type I, $2$ = type II, $3$ = type X,
$4$ = type Y, $5$ = Flavour-Aligned, $6$ = general \THDM.  The input
entries 21, 22, and 23 in the \lstinline|MINPAR| block can be used to
set the values of $\zeta_u$, $\zeta_d$ and $\zeta_l$ in the
Flavour-Aligned \THDM, and are ignored for all other types.
Additional blocks for the general case are described below.

\paragraph{Gauge basis input parameters}

Alternatively, the input can be given in the ``gauge basis'' in a
format compatible with \THDMC\ \cite{Eriksson:2009ws}.  In the ``gauge
basis'' the \THDM\ parameters must be given in the \lstinline|MINPAR|
block.  The available ``gauge basis'' input parameters are listed in
\tabref{tab:basis2}.  The following shows an example input in the
``gauge basis'':
%
\begin{lstlisting}
Block MINPAR                  # model parameters in gauge basis
    3        3                # tan(beta)
   11        0.7              # lambda_1
   12        0.6              # lambda_2
   13        0.5              # lambda_3
   14        0.4              # lambda_4
   15        0.3              # lambda_5
   16        0.2              # lambda_6
   17        0.1              # lambda_7
   18        40000            # m_{12}^2
   21        0                # zeta_u (only used if Yukawa type = 5)
   22        0                # zeta_d (only used if Yukawa type = 5)
   23        0                # zeta_l (only used if Yukawa type = 5)
   24        2                # Yukawa type (1, 2, 3, 4, 5 = aligned, 6 = general)
\end{lstlisting}
%
Two parametrizations for the general \THDM\ are implemented, see
Eq.~\eqref{eq:rho}. The first option is a deviation from the \FATHDM\
(or types I, II, X, Y), parametrized by the additional matrices
$\Delta_f$. Thus, after choosing Yukawa type = 1, \ldots, 5, i.e.\
type I, II, X, Y and \FATHDM, the matrices $\Delta_u$, $\Delta_d$ and
$\Delta_l$ can be optionally given in the following dedicated blocks:
%
\begin{lstlisting}
Block GM2CalcTHDMDeltauInput
    1 1   0                    # Re(Delta_u(1,1))
    1 2   0                    # Re(Delta_u(1,2))
    1 3   0                    # Re(Delta_u(1,3))
    2 1   0                    # Re(Delta_u(2,1))
    2 2   0                    # Re(Delta_u(2,2))
    2 3   0                    # Re(Delta_u(2,3))
    3 1   0                    # Re(Delta_u(3,1))
    3 2   0                    # Re(Delta_u(3,2))
    3 3   0                    # Re(Delta_u(3,3))
Block GM2CalcTHDMDeltadInput
    1 1   0                    # Re(Delta_d(1,1))
    1 2   0                    # Re(Delta_d(1,2))
    1 3   0                    # Re(Delta_d(1,3))
    2 1   0                    # Re(Delta_d(2,1))
    2 2   0                    # Re(Delta_d(2,2))
    2 3   0                    # Re(Delta_d(2,3))
    3 1   0                    # Re(Delta_d(3,1))
    3 2   0                    # Re(Delta_d(3,2))
    3 3   0                    # Re(Delta_d(3,3))
Block GM2CalcTHDMDeltalInput
    1 1   0                    # Re(Delta_l(1,1))
    1 2   0                    # Re(Delta_l(1,2))
    1 3   0                    # Re(Delta_l(1,3))
    2 1   0                    # Re(Delta_l(2,1))
    2 2   0.1                  # Re(Delta_l(2,2))
    2 3   0                    # Re(Delta_l(2,3))
    3 1   0                    # Re(Delta_l(3,1))
    3 2   0                    # Re(Delta_l(3,2))
    3 3   0                    # Re(Delta_l(3,3))
\end{lstlisting}
%
The other option for the general \THDM\ corresponds to the $\Pi$ parametrization, implemented when setting Yukawa type = 6. In this case, the input parameters $\Delta_u$, $\Delta_d$ and $\Delta_l$ are
ignored and the real parts of the matrices
$\Pi_u$, $\Pi_d$ and $\Pi_l$ can be given in the following dedicated
blocks:
%
\begin{lstlisting}
Block GM2CalcTHDMPiuInput
    1 1   0                    # Re(Pi_u(1,1))
    1 2   0                    # Re(Pi_u(1,2))
    1 3   0                    # Re(Pi_u(1,3))
    2 1   0                    # Re(Pi_u(2,1))
    2 2   0                    # Re(Pi_u(2,2))
    2 3   0                    # Re(Pi_u(2,3))
    3 1   0                    # Re(Pi_u(3,1))
    3 2   0                    # Re(Pi_u(3,2))
    3 3   0                    # Re(Pi_u(3,3))
Block GM2CalcTHDMPidInput
    1 1   0                    # Re(Pi_d(1,1))
    1 2   0                    # Re(Pi_d(1,2))
    1 3   0                    # Re(Pi_d(1,3))
    2 1   0                    # Re(Pi_d(2,1))
    2 2   0                    # Re(Pi_d(2,2))
    2 3   0                    # Re(Pi_d(2,3))
    3 1   0                    # Re(Pi_d(3,1))
    3 2   0                    # Re(Pi_d(3,2))
    3 3   0                    # Re(Pi_d(3,3))
Block GM2CalcTHDMPilInput
    1 1   0                    # Re(Pi_l(1,1))
    1 2   0                    # Re(Pi_l(1,2))
    1 3   0                    # Re(Pi_l(1,3))
    2 1   0                    # Re(Pi_l(2,1))
    2 2   0.1                  # Re(Pi_l(2,2))
    2 3   0                    # Re(Pi_l(2,3))
    3 1   0                    # Re(Pi_l(3,1))
    3 2   0                    # Re(Pi_l(3,2))
    3 3   0                    # Re(Pi_l(3,3))
\end{lstlisting}
%
The input parameters $\Pi_u$, $\Pi_d$ and $\Pi_l$ are ignored when choosing Yukawa type
= 1, \ldots, 5, i.e.\ type I, II, X, Y and \FATHDM.
%The input parameters $\Pi_u$, $\Pi_d$ and $\Pi_l$ are ignored in the
%aligned \THDM s (type I, II, X, Y and flavour-aligned, i.e.\ Yukawa type
%= 1, \ldots, 5).

The SLHA-like interface described so far is very convenient and
intuitive to use and it does not require any prior knowledge of
programming languages to run \GMTCalc\ this way. The execution time
for this is also very short, around $5\unit{ms}$ per point on current
laptops.\footnote{Based a machine with an Intel(R) Core(TM) i7-5600U
  CPU @ 2.60GHz processor.}  Nevertheless even faster execution times
and easy interfacing with other existing calculators and sampling
algorithms are enabled through our C++ ($0.05\unit{ms}$), C
($0.05\unit{ms}$), \mathematica\ ($0.5\unit{ms}$) and \python\
($0.08\unit{ms}$) interfaces.  In the following subsections we describe
how to use each of these interfaces. 

\subsection{Running \GMTCalc\ from within C++}
\label{sec:c++_interface}

\GMTCalc\ provides a C++ programming interface, which allows for
calculating of $\amuBSM$ in the \THDM\ up to the two-loop level.  The
following C++ source code snippet shows a two-loop example calculation
in the \THDM\ of type II with input parameters defined in the ``mass
basis'', see \tabref{tab:basis}.
%
\lstinputlisting[language=c++]{anc/example_mass-basis.cpp}
%
This example source code can be compiled as follows (assuming
\GMTCalc\ has been compiled to a shared library
\lstinline|libgm2calc.so| on a UNIX-like operating system with
\lstinline|g++| installed):
%
\begin{lstlisting}[language=bash]
g++ -I${GM2CALC_DIR}/include/ -I${EIGEN_DIR} example.cpp ${GM2CALC_DIR}/build/lib/libgm2calc.so
\end{lstlisting}%$
%
Here \lstinline|example.cpp| is the file that contains the above
listed source code.  The variable \lstinline|GM2CALC_DIR| contains the
path to the \GMTCalc\ root directory and \lstinline|EIGEN_DIR|
contains the path to the Eigen library header files.  Running the
created executable \lstinline|a.out| yields
%
\begin{lstlisting}[language=bash]
$ ./a.out
amu = 1.67323e-11 +- 3.3616e-12
\end{lstlisting}%$
%
In line~12 of the example source code an object of type
\lstinline|Mass_basis| is created, which contains the input parameters
in the mass basis, see \tabref{tab:basis}.  The mass basis input
parameters are set in lines 13--31.  Note that the Yukawa type is set
to type II in line~13, which implies that given values of $\zeta_f$
are ignored and internally fixed to the values given in
\tabref{tab:zeta}.  
Additionally the inputs $\Pi_f$ and $\Delta_f$ are unused for this type, and ignored.  
%
In line~34 an object of type \lstinline|SM| is created that contains
all \SM\ input parameters.  The \SM\ input parameters are set to
reasonable default values from the PDG \cite{Zyla:2020zbs}.  In
lines~35--39 the values for $\aem(m_Z)$, $m_t$, $m_c^\MS(2\GeV)$,
$m_b^\MS(m_b^\MS)$ and $m_\tau$ are set to specific values.
%
In line~42 an object of type \lstinline|Config| is created, which
contains the options to customize the calculation of $\amuBSM$ and
$\Damu$.  In line~43 the ``running masses'' scheme is chosen, see
\secref{sec:running_couplings}.
%
In line~47 the \THDM\ model is created, given the \THDM\ and \SM\ input
parameters and the configuration options defined above.  The value of
$\amuBSM = \amu^\ol + \amu^\tl$ is calculated in lines~50--51.  The
corresponding uncertainty $\Damu$ is calculated in line~54--55.  The
values $\amuBSM$ and $\Damu$ are printed in line~57.
%
Note that the \THDM\ model should be created within a \lstinline|try|
block, because the constructor of the \THDM\ class throws an exception
if a physical problem occurs (e.g.\ a tachyon) or an input parameter
has been set to an invalid value, see \tabref{tab:basis2}.

Alternatively, the \THDM\ input parameters can be given in the ``gauge
basis'', i.e.\ in terms of the Lagrangian parameters, see
\tabref{tab:basis}.  The following C++ source code snippet shows a
corresponding example two-loop calculation in the \THDM\ of type II,
where the input parameters are defined in the ``gauge basis''.
%
\lstinputlisting[language=c++]{anc/example_gauge-basis.cpp}
%
In line~12 an object of type \lstinline|Gauge_basis| is created, which
is filled with the gauge basis input parameters in lines~13--25.  With
the defined gauge basis input parameters the calculation of $\amuBSM$
and $\Damu$ continues as in the mass basis example above.


\subsection{Running \GMTCalc\ from within C}

Alternatively to the C++ programming interface detailed in
\secref{sec:c++_interface}, \GMTCalc\ also provides a C programming
interface.  The following C source code snippet shows a two-loop
example calculation in the \THDM\ of type II with input parameters
defined in the ``mass basis'', see \tabref{tab:basis}.
%
\lstinputlisting[language=c]{anc/example_mass-basis.c}
%
This example source code can be compiled as follows (assuming
\GMTCalc\ has been compiled to a shared library
\lstinline|libgm2calc.so| on a UNIX-like operating system with
\lstinline|gcc| installed):
%
\begin{lstlisting}[language=bash]
gcc -I${GM2CALC_DIR}/include/ example.c ${GM2CALC_DIR}/build/lib/libgm2calc.so
\end{lstlisting}%$
%
Here \lstinline|example.c| is the file that contains the above
listed source code.

The C example source code is very similar to the mass basis C++
example.  In line~12 an object of type
\lstinline|gm2calc_THDM_mass_basis| is created and filled with the
mass basis input parameters in lines~13--32.  The Yukawa type of the
model is defined in line~13 to be type II.
%
In line~35 an object of type \lstinline|gm2calc_SM| is created, which
contains the \SM\ input parameters.  The \SM\ input parameters are set to
their default values in line~36.  In lines~37--41 the values of
$\aem(m_Z)$, $m_t$, $m_c^\MS(2\GeV)$, $m_b^\MS(m_b^\MS)$ and
$m_\tau$ are set to specific values.
%
In line~44 a config object of type \lstinline|gm2calc_THDM_config| is
created which contains options to customize the calculation.  These
options are set to default values in line~45.
%
In line~48 a null-pointer to a \THDM\ model of type
\lstinline|gm2calc_THDM| is created.  In line~49 the \THDM\ model is
created and the pointer is set to point to the model.  If an error
occurrs, the pointer is set to 0 and the returned \lstinline|error|
variable is set to a value that is not \lstinline|gm2calc_NoError|.
%
If no error has occurred, the example continues to calculate $\amuBSM$
and $\Damu$ in lines~53--58, respectively.  The result is printed in
line~60.  In line~65 the memory reserved for the \THDM\ model is freed.

Alternatively, the \THDM\ input parameters can be given in the ``gauge
basis'', similarly to the gauge basis C++ example.  The following C
source code snippet shows a corresponding example two-loop calculation
in the \THDM\ of type II, where the input parameters are defined in the
``gauge basis''.
%
\lstinputlisting[language=c]{anc/example_gauge-basis.c}
%
In line~12 an object of type \lstinline|gm2calc_THDM_gauge_basis| is
created, which is filled with the gauge basis input parameters in
lines~13--26.  In line~43 the \THDM\ model is created, using the gauge
basis input parameters.  The calculation of $\amuBSM$ and $\Damu$ is
performed in lines~47--52, as in the ``mass basis'' example above.


\subsection{Running \GMTCalc\ from within \mathematica}

\GMTCalc\ can be run from within \mathematica\ using the
\lstinline|MathLink| interface.  The following source code snippet shows
an example calculation of $\amuBSM$ and its uncertainty at the two-loop
level using input parameters given in the ``gauge basis''.
%
\lstinputlisting[language=mathematica]{anc/example_gauge-basis.m}
%
In line~1 \GMTCalc's \lstinline|MathLink| executable
\lstinline|bin/gm2calc.mx|, which is created when building \GMTCalc,
is loaded into the \mathematica\ session.  In lines~4--6 two
configuration options to customize the calculation are set: The
calculation shall be performed at the two-loop level using the
``running masses'' scheme defined in \secref{sec:running_couplings}.
%
In lines~9--29 the \SM\ input parameters are defined.  Unset parameters
are set to reasonable default values, see
\lstinline|Options[GM2CalcSetSMParameters]|.  In lines~32--46 the
values of $\amuBSM$ and $\Damu$ are calculated using the function
\lstinline|GM2CalcAmuTHDMGaugeBasis|, which takes the gauge basis
input parameters as arguments, see \tabref{tab:basis2}.  The result is
printed in line~48.

Alternatively, the calculation can be performed using input parameters
given in the ``mass basis''.  The following source code snippet shows
an example calculation of $\amuBSM$ and its uncertainty at the two-loop
level using input parameters given in the ``mass basis''.
%
\lstinputlisting[language=mathematica]{anc/example_mass-basis.m}
%
The calculation of $\amuBSM$ and $\Damu$ is performed in lines~32--51
with the function \lstinline|GM2CalcAmuTHDMMassBasis|, which takes the
mass basis input parameters as arguments, see \tabref{tab:basis2}.
The result is printed in line~53.

\subsection{Running \GMTCalc\ from within \python}
\label{sec:python_interface}

Newly implemented in \GMTCalc\ 2.0.0 is the ability to interface with \python\ using the package \lstinline|cppyy|.  An example calculation using the interface is shown in the code snippet below, working in the ``mass basis''.
%
\lstinputlisting[language=python]{anc/example_mass_basis.py}
%
Note similarity between the above code and the C++ and C interfaces.
Line~3 is to make sure that this example which is written using
\python\ 3-style \lstinline|print| functions can still work in
\python\ 2.  Line~4 imports the interface script
\lstinline|gm2_python_interface| which loads the \lstinline|cppyy| and
\lstinline|os| packages, as well as the header and library
locations.  This interface file is originally in the \lstinline|src| subdirectory.  
After performing
%
\begin{lstlisting}[language=bash]
cmake -DBUILD_SHARED_LIBS=ON .. 
\end{lstlisting}
%
the interface script will be copied into the subdirectory
\lstinline|bin|, and it will be filled with the path information 
for the \GMTCalc\ headers, library, and the \lstinline|Eigen3| 
path.  The interface can be imported from there by other \python\ scripts, 
or moved to an appropriate location where the user has their own \python\ scripts.  
Lines~6--11 load the relevant header
files, and line~13 loads the \GMTCalc\ shared library.  In
lines~16--20 the necessary namespaces from C++ are loaded into
\python.  In line~23 the \THDM\ \lstinline|Mass_basis|
object is initialized, while on line~24 the \THDM\ is specified to be
type II.  Lines~25--36 involve setting the values for simple
attributes in the basis.  Lines~36--42 assign values to the
\lstinline|Eigen::Matrix| attributes, however since these are meant to
be ignored, they are just set to $0$.  Lines~44--49 initialize an
\lstinline|SM| object and ensures it has the appropriate parameters.
Line 51 initializes the \lstinline|config| object, which is used to
flag the use of running coupling in the next line.  Line~56
initializes a \lstinline|THDM| object using the
\lstinline|Mass_basis|, \lstinline|SM|, and \lstinline|Config|
information.  Lines~58--63 prints out the values of $\amuBSM$ and
$\Damu$ which are calculated using the interface functions
\lstinline|calculate_amu_1loop|, \lstinline|calculate_amu_2loop|, and
\lstinline|calculate_uncertainty_amu_2loop|.  Alternatively an error
message will be printed out on line~63 should a problem arise.


Another example of the \python\ interface is shown below, this time using the ``gauge basis'':
%
\lstinputlisting[language=python]{anc/example_gauge_basis.py}
%
In line~23 we instead initialize a \lstinline|Gauge_basis| object.  To define the attribute \lstinline|lambda|, we need to circumvent \python's reserved keywords.  This is done by defining a $7\times1$ \lstinline|Eigen::Matrix| in line~26.  This \lstinline|Matrix| is initialized to $0$ before assigned the appropriate entires elementwise on lines~28--34.  Then the method \lstinline|__setattr__| can be used to interface the values to the C++ code.  Then the other values can be defined on lines~36--53, and finally the result for $\amuBSM$ is printed on line~65.
