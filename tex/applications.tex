\section{Applications}
\label{sec:applications}

\subsection{Parameter scan in the type II and X models}

As an application we perform a 2-dimensional parameter scan over $m_A$
and $\tan\beta$ for the type II and type X \THDM\ models,
similarly to Ref.\ \cite{Broggio:2014mna}.  However, in contrast to
Ref.\ \cite{Broggio:2014mna} we include the two-loop bosonic
contributions and use the updated value of
$\amuBSM = (25.1 \pm 5.9)\times 10^{-10}$ from Eq.\
\eqref{eq:amuBSM}.  The following C++ source code shows the program to
perform the scan.
%
\lstinputlisting[language=c++]{anc/mA-tb.cpp}
%
The function \lstinline|calc_amu| calculates $\amuBSM$ in the
\THDM\ at the two-loop level for a given value of $m_A$ and
$\tan\beta$ and a specified Yukawa type.  The remaining \THDM\ input
parameters in the mass basis are set to $m_h=126\GeV$,
$m_H=m_{H^\pm}=200\GeV$, $\sin(\beta-\alpha)=1$,
$\lambda_6=\lambda_7=0$,
$m_{12}^2=m_H^2/\tan\beta + (m_h^2 - \lambda_1 v^2)/\tan^3\beta$ and
$\lambda_1=\sqrt{4\pi}$.  In the \lstinline|main| function the loop
over $m_A$ and $\tan\beta$ is performed and $\amuBSM$ is
calculated for the type II and type X \THDM\ and the result is written
to the standard output.
%
The 2-dimensional output is shown in \figref{fig:mA-tb} for the two
types of the \THDM.
%
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.49\textwidth]{img/mA-tb-2}\hfill
  \includegraphics[width=0.49\textwidth]{img/mA-tb-X}
  \caption{Two-loop prediction of $\amuBSM$ in the \THDM\ of type
    II (left) and type X (right) as a function of $\tan\beta$ and
    $m_A$ with $m_h=126\GeV$, $m_H=m_{H^\pm}=200\GeV$,
    $\sin(\beta-\alpha)=1$, $\lambda_6=\lambda_7=0$,
    $m_{12}^2=m_H^2/\tan\beta + (m_h^2 - \lambda_1 v^2)/\tan^3\beta$
    and $\lambda_1=\sqrt{4\pi}$.  In the green, yellow and gray
    regions the \THDM\ predicts the correct value of
    $\amuBSM = (25.1 \pm 5.9)\times 10^{-10}$ from Eq.\
    \eqref{eq:amuBSM} within one, two and three standard deviations,
    respectively.}
  \label{fig:mA-tb}
\end{figure}


\subsection{Size of fermionic and bosonic contributions}
\label{sec:FB}

In the following we illustrate the calculation of the two-loop
fermionic and bosonic contributions, $\amuF$ and $\amuB$, separately.
For the illustration we perform a scan over $m_A$ for the demo
parameter scenario from \THDMC\ \cite{Eriksson:2009ws}, which is a
type II \THDM\ scenario where $m_H= 400\GeV$, $m_{H^\pm}=440\GeV$,
$\tan\beta=3$, $\sin(\beta-\alpha)=0.999$, $\lambda_6=\lambda_7=0$ and
$m_{12}^2=(200\GeV)^2$.  The following C source code shows the program
to perform the scan.
%
\lstinputlisting[language=c]{anc/FB.c}
%
The \SM\ input parameters and the configuration options are set to their
default values by passing 0 as the last two arguments to the function
\lstinline|gm2calc_thdm_new_with_mass_basis| that creates the \THDM\
model in line~39.  The individual bosonic and fermionic contributions
are calculated in lines~42--43 and written to the standard output in
line~45.
%
\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{img/FB}\hfill
  \includegraphics[width=0.49\textwidth]{img/FB_FATHDM}
  \includegraphics[width=0.49\textwidth]{img/running}\hfill
  \includegraphics[width=0.49\textwidth]{img/running_FATHDM}
  \caption{Different contributions to $\amuBSM$ in the \THDM. In the
    left panels we show scenarios from the type II \THDM\ as a
    function of $m_A$ with $m_H= 400\GeV, m_{H^\pm}=440\GeV$,
    $\tan\beta=3$, $\sin(\beta-\alpha)=0.999$, $\lambda_6=\lambda_7=0$
    and $m_{12}^2=(200\GeV)^2$.  In the right panels we show
    \FATHDM\ scenarios with $m_h = 125\GeV$, $m_H=150\GeV$,
    $m_{H^\pm}=150\GeV$, $\sin(\beta-\alpha)=0.999$, $\lambda_6=0$,
    $\lambda_7=0$, $\tan\beta = 2$, $m_{12}^2$ is fixed to avoid
    $h\rightarrow AA$ decays using a polynomial
    equation shown in the related source code, $\zeta_u=\zeta_d =
    -0.1$, $\zeta_l = 50$ based on scenarios found for \figurename~10
    of Ref.\ \cite{Cherchiglia:2017uwv}. In the top panels we show
    fermionic (red solid line) and bosonic (blue dashed-dotted line)
    two-loop contributions separately.  The top right panel also shows
    a purple region over the values of $m_A$ where it is possible to
    explain Eq.\ \eqref{eq:amuBSM}.  In the bottom panels we compare
    the fermionic two-loop contributions, when the running 3rd
    generation fermion masses shown in \eqref{eq:running_masses} are
    used (red solid line) and when the 3rd generation input masses
    from \eqref{eq:input_masses} are used (green dashed line).  The
    red and green bands show the corresponding uncertainties.  The
    bottom left panel also shows two-loop fermionic contributions
    calculated from \THDMC\ as a black dotted line.  
    *This is not a vanilla \THDMC\ calculation, the
    \SM-Higgs-like two-loop contributions have been removed, and the two-loop 
    charged Higgs Barr-Zee contributions have been added as explained in the text.  }
  \label{fig:FB}
\end{figure}
%
In the top left panel of \figref{fig:FB} we show the results for the
fermionic (red solid line) and bosonic (blue dashed-dotted line)
contributions as a function of $m_A$ for this demo scenario from
\THDMC.  For the shown parameter space the fermionic contributions
decrease, while the bosonic contributions increase for increasing
$m_A$.  Furthermore, the bosonic contributions are negative, while the
fermionic contributions are positive, which leads to a partial
cancellation of the two contributions.  However, as is generally the
case for the type II \THDM\ scenarios that are not already excluded by
other constraints, the scenarios we plot here cannot explain the large
deviation between the Standard Model prediction and experiment given
in Eq.\ \eqref{eq:amuBSM}.

If instead we consider the \FATHDM\ scenarios, as in the top right panel
of \figref{fig:FB} it is now possible to explain large deviations.
Here we fix the following parameters $m_h = 125\GeV$, $m_H=150\GeV$,
$m_{H^\pm}=150\GeV$, $\sin(\beta-\alpha)=0.999$, $\lambda_6=0$,
$\lambda_7=0$, $\tan\beta = 2$, $\zeta_u=\zeta_d=-0.1$,
$\zeta_l = 50$ based on results used for \figurename~10
of Ref.\ \cite{Cherchiglia:2017uwv}. It should be noticed that,
  once the masses for the charged and CP-even scalar are close
  together and below around 300 GeV, electroweak as well as unitarity
  and perturbativity constraints can be evaded for arbitrarily low
  values of $m_A$ \cite{Broggio:2014mna}. The input parameters 
  here are chosen accordingly.
Since $m_A < m_{h_\SM}/2$, it is possible for $h \rightarrow A A$ decays to occur
unless we enforce the coupling $C_{hAA}=0$.\footnote{The coupling
    $C_{hAA}$ has been discussed in detail 
    in Ref.\ \cite{Cherchiglia:2017uwv}. 
    Setting $C_{hAA}=0$ directly corresponds to the choice $\Lambda_6=0$, where
    $\Lambda_6$ is a potential parameter in the so-called Higgs basis,
    and to a specific relation between all potential parameters in the
    general basis. It has been checked that
    although non-null values for $C_{hAA}$ can be allowed, they are
    experimentally strongly constrained. Thus, for simplicity, we have
    adopted $C_{hAA}=0$ in our analysis, which automatically
    guarantees that $\lambda_1$ (or equivalently $m_{12}^2$) will not
    be chosen in a region excluded by experimental constraints or by
    unitarity or perturbativity.}  This fixes the value of $\lambda_1$ 
according to Eq.\ (12) in Ref.\ \cite{Cherchiglia:2017uwv}, 
which can be set in the mass basis using $m_{12}^2$ and applying the relations in
Eqs.\ (2.12)--(2.13) in Ref.\ \cite{Cherchiglia:2016eui}.  
This leads to the fitted 2nd-order polynomial relation with a dependence on $m_A$ seen in the source code below.  

These scenarios have a very light pseudoscalar mass, but LHC limits
are much weaker compared to the type II case and can be evaded for
these scenarios.  The two-loop fermion contributions rise rapidly as
the pseudoscalar mass decreases, dominating over the two-loop bosonic
contributions, though the latter are just large enough to have an
impact on constraints from $\amu$.  Note that for higher values of
$m_A$ it is possible to get larger bosonic contributions as can be
seen in \figurename~10 of Ref.\ \cite{Cherchiglia:2017uwv}.  In the
scenarios we plot here the one-loop contributions, which are not shown
in \figref{fig:FB}, have a negative effect on the contributions, with
a size of approximately one-third of the two-loop fermionic
contributions.  Thus it can be seen that $\amuBSM$ can be explained
with a very low $m_A$ for the values of $m_A$ in the purple region.
The scan for this scenario can be performed with the following C
source code:
%
\lstinputlisting[language=c]{anc/FB_FATHDM.c}
%
% (Note, that \THDMC\ does not include bosonic contributions.  This
% explains why \GMTCalc\ does not agree by default with \THDMC.)


\subsection{Running fermion masses}

In this subsection we study the effect of using the input vs.\ running
fermion masses in the two-loop fermionic contributions as described in
\secref{sec:running_couplings}, and compare the results with \THDMC.  
The following \mathematica\ source
code shows a program to perform a scan over $m_A$ using the same type II \THDM\
parameter region as in \secref{sec:FB}.
%
\lstinputlisting[language=mathematica]{anc/running.m}
%
The function \lstinline|CalcAmu| calculates the two-loop fermionic
contribution $\amuF$ and the uncertainty $\Damu$ for a given value of
$m_A$ and the mass basis input parameters defined above.  In
line~20 the usage of running fermion masses is disabled and the
calculation is performed in the subsequent line.  Similarly, in
line~24 the usage of running fermion masses is enabled and the
calculation is performed in the subsequent line.  The results are
collected in the variable \lstinline|data| and are exported to a file
in line~31.  We also used a very similar script to do the same
calculations for the \FATHDM\ scenarios discussed in \secref{sec:FB}.

The effect of using running fermion masses in the two-loop fermionic
contributions is shown in \figref{fig:FB} for the type II (bottom left
panel) and flavour aligned (bottom right panel) scenarios matching
those described in the previous section.  The red solid line shows the
value of $\amuF$ when the running masses \eqref{eq:running_masses} are
used, i.e.\ the 3rd generation fermion masses are run to the scale of
the Higgs boson in the two-loop fermionic Barr-Zee Feynman diagrams.
Note that although the vertical axes are slightly different,
the red lines shown in the bottom panels are identical to the red
lines from the corresponding panels immediately above them, which
were discussed in the previous section. The green dashed lines show
the value of $\amuF$ when input fermion masses listed in
\eqref{eq:input_masses} are used.  In both scenarios that we look at
the value of $\amuF$ is smaller when running masses are used,
though the difference is only distinguishable for the type II case where the
size of the contributions is much smaller.  The reason for this is
that due to the negative fermion mass $\beta$ functions the running
masses are numerically smaller than the corresponding input masses in
the shown scenarios, which leads to a systematic reduction of the
fermionic two-loop contributions.

In addition to the red solid and green dashed lines from \GMTCalc, as
the scenario in question is a benchmark point for
\THDMC\ \cite{Eriksson:2009ws}, we show in the bottom left panel of
\figref{fig:FB} the corresponding result obtained with \THDMC\ 1.8.0
as black dotted line.  Note that at the two-loop level
\THDMC\ includes only fermionic contributions to $\amuBSM$.
Furthermore, \THDMC\ does not subtract the
contributions from the \SM\ Higgs boson and does not include the
two-loop contributions from the charged Higgs boson.  Therefore to
obtain this black dotted line we have thus subtracted the
two-loop \SM\ Higgs contributions from the \THDMC\ result and added
the two-loop contributions from the charged Higgs boson.  Since
\THDMC\ inserts running fermion masses into the fermionic
contributions, the black dotted line can be compared to the red solid
line in the figure.  There is a small deviation between these two
lines, which originates from the inclusion of fermionic Barr-Zee
diagrams with an internal $Z$ boson in \GMTCalc, which are not
included in \THDMC.

In the bottom panels of \figref{fig:FB} we also show the uncertainties
calculated with \eqref{eq:damu} as lighter shaded regions of the
corresponding color about the red and green lines. In the bottom left
panel the red and green lines both lie within the uncertainty estimate
for the alternative prediction (shaded green and shaded red regions
respectively) for all values of $m_A$ plotted.  This is also true in
the bottom right panel though there is no visible distinction between
the red and green lines or their uncertainties here. Since the
difference between the lines is of higher order, this indicates that
our uncertainty estimate is working as expected and accounts for the
expected higher order corrections.
