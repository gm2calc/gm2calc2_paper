\section{Review of 2HDM and implemented contributions to $\amu$} 
\label{sec:model}

\subsection{The Model}

The Two-Higgs Doublet Model (\THDM) extends the Standard Model (\SM)
with an additional scalar $\SU(2)$ doublet. We denote the two complex
Higgs $\SU(2)$ doublets in the \THDM\ as $\Phi_i$ ($i=1,2$),
%
\begin{align}
  \Phi_i =
  \begin{pmatrix}
    a_i ^+ \\ \frac{1}{\sqrt{2}} \left( v_i + b_i + \imath c_i \right)
  \end{pmatrix},
  \label{eq:higgsdoublets}
\end{align}
%
where $b_i$ and $c_i$ are real scalar fields and $a_i^+$ are complex scalar fields. 
Each Higgs doublet acquires a real non-zero vacuum expectation value (VEV), $v_i$, which satisfy
%
\begin{align}
  \tan\beta &= \frac{v_2}{v_1}, &
  v^2 &= v_1^2 + v_2^2,
  \label{eq:vevs} 
\end{align}
%
with $0 \leq \beta \leq \pi/2$ and $v \approx 246\GeV$, which implies
$v_1 = v\cos\beta$ and $v_2 = v\sin\beta$.
%
The extended Lagrangian includes the Higgs potential $\mathcal{L}_{\text{Scalar}}$ and the Yukawa interaction part $\mathcal{L}_{\text{Yuk}}$,
%
\begin{align}
  \mathcal{L} &\ni \mathcal{L}_\text{Scalar} + \mathcal{L}_\text{Yuk} . 
\end{align}
%
The most general form of Higgs potential is 
%
\begin{align}
\begin{split}
  -\mathcal{L}_\text{Scalar} ={}&
  m_{11}^2 \Phi_1^\dagger \Phi_1
  + m_{22}^2 \Phi_2^\dagger \Phi_2
  - \left[ m_{12}^2 \Phi_1^\dagger \Phi_2 + \hc \right] \\
  &+ \frac{1}{2} \lambda_1 \left( \Phi_1^\dagger \Phi_1 \right)^2
   + \frac{1}{2} \lambda_2 \left( \Phi_2^\dagger \Phi_2 \right)^2
   + \lambda_3 \left( \Phi_1^\dagger \Phi_1 \right)\left( \Phi_2^\dagger \Phi_2 \right)
   + \lambda_4 \left( \Phi_1^\dagger \Phi_2 \right)\left( \Phi_2^\dagger \Phi_1 \right) \\
  &+ \left[
    \frac{1}{2} \lambda_5 \left( \Phi_1^\dagger \Phi_2 \right)^2
    + \lambda_6 \left( \Phi_1^\dagger \Phi_1 \right)\left( \Phi_1^\dagger \Phi_2 \right)
    + \lambda_7 \left( \Phi_2^\dagger \Phi_2 \right)\left( \Phi_1^\dagger \Phi_2 \right)
    + \hc
  \right] , 
\end{split} 
\label{eq:Lscal}
\end{align}
%
where $\lambda_5$, $\lambda_6$, $\lambda_7$ and $m_{12} ^2$ are real for the $CP$-conserving Higgs potential.
%
The mass eigenstates of Higgs and Goldstone bosons $h$, $H$, $A$, $H^+$, $G^0$ and $G^+$ are obtained as
%
\begin{align}
  \begin{pmatrix} h \\ H \end{pmatrix} &= Z_{\mathcal{H}^0} \mathcal{H}^0 ,&
  \begin{pmatrix} G^0 \\ A \end{pmatrix} &= Z_{\mathcal{A}} \mathcal{A} ,&
  \begin{pmatrix} G^+ \\ H^+ \end{pmatrix} &= Z_{\mathcal{H}^+} \mathcal{H}^+ ,
  \label{eq:eigenstate1}
\end{align}
%
with 
\begin{align}
  \mathcal{H}^0 &=
  \begin{pmatrix}
    b_1 \\ b_2
  \end{pmatrix}, &
  \mathcal{A} &=
  \begin{pmatrix}
    c_1 \\ c_2
  \end{pmatrix} ,&
  \mathcal{H}^+ &=
  \begin{pmatrix}
    a_1^+ \\ a_2^+
  \end{pmatrix},
  \label{eq:gaugestate1}
\end{align}
%
from Eq.~\eqref{eq:Lscal}.  The orthogonal matrices
$Z_{\mathcal{H}^0}$,
$Z_{\mathcal{A}}$ and
$Z_{\mathcal{H}^\pm}$ for
Eq.~\eqref{eq:eigenstate1} are given as
%
\begin{align}
  Z_{\mathcal{H}^0} &=
  \begin{pmatrix}
    -\sin\alpha & \cos\alpha \\
    \cos\alpha & \sin\alpha
  \end{pmatrix} ,&
  Z_{\mathcal{A}} &=
  \begin{pmatrix}
    \cos\beta & \sin\beta \\
    -\sin\beta & \cos\beta
  \end{pmatrix} ,&
  Z_{\mathcal{H}^+} &=
  \begin{pmatrix}
    \cos\beta & \sin\beta \\
    -\sin\beta & \cos\beta
  \end{pmatrix} .
\end{align}
%
The corresponding mass square matrices $M_{\mathcal{H}^0} ^2$,
$M_{\mathcal{A}} ^2$ and $M_{\mathcal{H}^+} ^2$ for $\mathcal{H}^0$,
$\mathcal{A}$ and $\mathcal{H}^+$ in Eq.~\eqref{eq:gaugestate1},
respectively, are diagonalized as
%
\begin{align}
  M_{\mathcal{H}^0} ^2 &= Z_{\mathcal{H}^0}^\dagger (M_{\mathcal{H}^0}^D) ^2 Z_{\mathcal{H}^0} ,&
  M_{\mathcal{A}} ^2 &= Z_\mathcal{A}^\dagger (M_{\mathcal{A}}^D) ^2 Z_\mathcal{A} , &
  M_{\mathcal{H}^+} ^2 &= Z_{\mathcal{H}^+}^\dagger (M_{\mathcal{H}^+}^D) ^2 Z_{\mathcal{H}^+} .
\end{align}
%
The Higgs boson mass eigenvalues are denoted as
%
\begin{align}
  M_{\mathcal{H}^0}^D &= \diag( m_h , m_H ), &
  M_{\mathcal{A}}^D &= \diag( m_{G^0} , m_A ), &
  M_{\mathcal{H}^\pm}^D &= \diag( m_{G^\pm} , m_{H^\pm} ),
\end{align}
%
where in Feynman gauge $m_{G^+}=m_W$ and $m_{G^0}=m_Z$.
%
We chose the $CP$-even Higgs boson mixing angle $\alpha$ such that
$-\pi/2 \leq \beta - \alpha \leq \pi/2$ \cite{Eriksson:2009ws}.


The general form of the Yukawa interaction Lagrangian is given as
%
\begin{align}
  -\mathcal{L}_\text{Yuk} ={}&
    \Gamma_d ^0\overline{q_L ^0}\Phi_1 d_R ^0
  + \Gamma_u ^0 \overline{q_L ^0}\Phi_1 ^{c} u_R ^0
  + \Gamma_l ^0 \overline{l_L ^0}\Phi_1 e_R ^0
  + \PiL_d \overline{q_L ^0}\Phi_2 d_R ^0
  + \PiL_u \overline{q_L ^0}\Phi_2 ^{c} u_R ^0
  + \PiL_l \overline{l_L ^0}\Phi_2 e_R ^0
  + \hc,
  \label{eq:Lyuk}
\end{align}
%
where $\Phi_i^{c} \equiv \imath \sigma^2 \Phi_i^\ast$, and
$q_L ^0$, $l_L ^0$, $u_R ^0$, $d_R ^0$, and $e_R ^0$ are fermion gauge eigenstates.%
\footnote{The $\Gamma^0$ and $\PiL$ matrices correspond to $\eta_{1}$ and $\eta_{2}$ in Ref.\ \cite{Branco:2011iw}, see their Eq.~(92).} 
In general the Yukawa coupling matrices,  $\Gamma_f ^0$ and $\PiL_f$ ($f=u,d,l$)
are complex and non-diagonal $3\times 3$ matrices which
can not be simultaneously diagonalized, which produces flavour changing neutral currents (FCNC).


From the Yukawa interaction Lagrangian in Eq.~\eqref{eq:Lyuk} the $3\times 3$
fermion mass matrices $M_f$ ($f=u,d,l$) are obtained as 
%
\begin{align}
  \label{eq:massdefup}
  M_u &= \frac{1}{\sqrt{2}} \left( v_1 \Gamma_u ^0 + v_2 \PiL_u \right), \\
  \label{eq:massdefdown}
  M_d &= \frac{1}{\sqrt{2}} \left( v_1 \Gamma_d ^0 + v_2 \PiL_d \right), \\
  \label{eq:massdeflepton}
  M_l &= \frac{1}{\sqrt{2}} \left( v_1 \Gamma_l ^0+ v_2 \PiL_l \right).
\end{align}
%
The fermion mass matrices are diagonalized using singular value
decomposition. The diagonal fermion mass matrices $M_f^D$ ($f=u,d,l$)
are given by
%
\begin{align}
  \label{eq:massdiagup}
  M_u &= V_u^\dagger M_u^D U_u , & &\text{ with } & M_u^D &= \diag( m_u , m_c , m_t) ,& \\
  \label{eq:massdiagdown}
  M_d &= V_d^\dagger M_d^D U_d , & &\text{ with } & M_d^D &= \diag( m_d , m_s , m_b) ,& \\
  \label{eq:massdiaglepton}
  M_l &= V_l^\dagger M_l^D U_l , & &\text{ with } & M_l^D &= \diag( m_e , m_\mu , m_\tau) , 
\end{align}
%
where $V_f$ and $U_f$ are unitary matrices,
and the corresponding fermion mass eigenstates $f=u,d,l$ are obtained as 
%
\begin{align}
  f _L &= V _f f_L ^0, &\text{and}& &f _R =U _f f_R ^0 .&
  \label{eq:fmasseigen}
\end{align}
%
By combining Eq.~\eqref{eq:vevs} and Eqs.\
\eqref{eq:massdefup}--\eqref{eq:massdeflepton}, $\Gamma_u ^0$, $\Gamma_d ^0$
and $\Gamma_l ^0$ can be expressed in terms of $M_f$ and $\PiL _f$ as
%
\begin{equation} \label{eq:gthdmfixyukawas}
	\Gamma_f ^0 = \frac{\sqrt{2} M_f}{v\cos\beta} - \PiL_f \tan\beta.
\end{equation}

The Yukawa interaction Lagrangian Eq.~\eqref{eq:Lyuk} can be also
written in the so-called Higgs basis, where only one Higgs doublet has
non-zero VEV. This can be obtained by rotating $\Phi_{1,2}$ and the
Yukawa matrices as
%
\begin{align}
  \begin{pmatrix}
    \Phi _v \\
    \Phi _\perp
  \end{pmatrix}
  &=
  \begin{pmatrix}
    \cos\beta & \sin\beta \\
    -\sin\beta & \cos\beta 
  \end{pmatrix}
  \begin{pmatrix}
    \Phi _1 \\
    \Phi _2
  \end{pmatrix},
  \label{eq:Higgsbasis}
  \\
  \begin{pmatrix}
    \Sigma _f ^0\\
    \rhoL _f
  \end{pmatrix}
  &=
  \begin{pmatrix}
    \cos\beta & \sin\beta \\
    -\sin\beta & \cos\beta
  \end{pmatrix}
  \begin{pmatrix}
    \Gamma _f ^0 \\
    \PiL _f
  \end{pmatrix} ,
\label{eq:yukrelation}
\end{align}
%
such that only $\Phi_v$ has the VEV $v$ and $\Sigma^0_f$ are its Yukawa
couplings.  It contains the \SM-like Goldstone bosons, whereas the second field 
$\Phi_\perp$ contains non-\SM\ Higgs bosons $H^\pm$ and $A$, and its
Yukawa couplings $\rhoL_f$ are important parameters in the
\THDM.\footnote{The $\Sigma^0$ and $\rhoL$ matrices correspond to
  $\eta$ and $\hat{\xi}$ in Ref.\ \cite{Branco:2011iw}, see their
  Eq.~(30).} 
With this transformation the Yukawa interaction Lagrangian becomes
%
\begin{align}
  -\mathcal{L}_\text{Yuk} ={}&
    \Sigma_d ^0 \overline{q_L ^0}\Phi_v d_R ^0
  + \Sigma_u ^0 \overline{q_L ^0}\Phi_v ^{c} u_R ^0
  + \Sigma_l ^0 \overline{l_L ^0}\Phi_v e_R ^0
  + \rhoL_d \overline{q_L ^0}\Phi_\perp d_R ^0
  + \rhoL_u \overline{q_L ^0}\Phi_\perp ^{c} u_R ^0
  + \rhoL_l \overline{l_L ^0}\Phi_\perp e_R ^0
  + \hc
  \label{eq:Lyukv}
\end{align}
%
In this way, the fermion masses are purely generated from the Higgs
doublet $\Phi_v$, and the fermion mass matrices in
Eqs.~\eqref{eq:massdefup}--\eqref{eq:massdeflepton} become  
%
\begin{align}
  M_u &= \frac{v}{\sqrt{2}}\Sigma_u ^0 , &M_d &= \frac{v}{\sqrt{2}}\Sigma_d ^0 , &M_l &= \frac{v}{\sqrt{2}}\Sigma _l ^0,
  \label{eq:fermionmassv}
\end{align}
%
which implies $V_f \Sigma_f^0 U_f^\dagger = \sqrt{2}M_f^D/v$.
In parallel to Eqs.~\eqref{eq:massdiagup}--\eqref{eq:massdiaglepton}
we may define Yukawa matrices in the basis of mass eigenstates as ($f=u,d,l$)
%
\begin{subequations}
  \begin{align}
  \Pi _f &\equiv V _f \PiL _f U _f ^\dagger, &
  \Sigma _f &\equiv V_f \Sigma _f ^0 U _f ^\dagger, \\
  \Gamma _f &\equiv V _f \Gamma _f^0 U _f ^\dagger, &
  \rho _f& \equiv V _f \rhoL _f U _f ^\dagger.
  \end{align}
\end{subequations}
%
Importantly, in the Higgs basis the $\Sigma _f ^0$ are diagonalized by the singular value decomposition
involving $V_f$ and $U_f$, but in general the $\rhoL _f$ are not. This
implies that Higgs-mediated FCNCs are induced by the coupling with the zero-VEV Higgs doublet $\Phi _\perp$. 

From Eqs.~\eqref{eq:massdiagup}--\eqref{eq:massdiaglepton},
\eqref{eq:yukrelation}, and \eqref{eq:fermionmassv} we obtain
%
\begin{align}
  \Pi _f &= \sqrt{2}\frac{M_f ^D}{v} \sin\beta + \rho _f \cos\beta ,
  \label{eq:Piprime}
\end{align}
%
where $\Pi _f \equiv V _f \PiL _f U _f ^\dagger$ and
$\rho _f \equiv V _f \rhoL _f U _f ^\dagger$.  The $\rho _f$ matrices
contain non-zero off-diagonal components, which reflects that in
general the two types of Yukawa coupling matrices $\Gamma _f ^0$ and
$\PiL _f$ can not be simultaneously diagonalized in the \THDM. The
$\rho _f$ term in Eq.~\eqref{eq:Piprime} can induce Higgs-mediated
flavour-changing neutral currents (FCNC) at the tree-level.
%The off-diagonal compoments of $\rho _f$ are non-zero, which leads to the FCNC at the tree-level.

\GMTCalc\ implements several variants of the \THDM\ with or without Higgs-mediated
FCNC:
%
\begin{itemize}
  \item The four well-known types with $Z_2$ symmetry: type I, type
    II, type X, type Y,
  \item
    the flavour-aligned \THDM\ (\FATHDM)~\cite{Pich:2009sp,Tuzon:2010vt},
    which also avoids tree-level Higgs-mediated FCNC and
    contains the four previous types as special cases,
  \item
    the \THDM\ with general Yukawa structures.
\end{itemize}
%
We will now describe these variants of the \THDM.  The most common way
to avoid FCNC is to impose a $Z_2$ symmetry, which leads to four
different types of Yukawa interactions in which specific subsets of
Yukawa couplings vanish. In the type I model all quarks and charged
leptons couple to $\Phi_2$, so we set
%
\begin{align}
  \Gamma_u ^0 = \Gamma_d ^0 = \Gamma_l ^0= 0.
\end{align}
%
In the type II model all up-type quarks couple to $\Phi_2$, while the
down-type quarks and charged leptons couple to $\Phi_1$, so we set
%
\begin{align}
  \Gamma_u ^0= \PiL_d = \PiL_l = 0.
\end{align}
%
In the type X model all quarks couple to $\Phi_2$, while the charged leptons
couple to $\Phi_1$, so we set
%
\begin{align}
  \Gamma_u ^0 = \Gamma_d ^0 = \PiL_l = 0.
\end{align}
%
In the type Y model the up-type quarks and charged leptons couple to $\Phi_2$, while the down-type quarks 
couple to $\Phi_1$, so we set
%
\begin{align}
  \Gamma_u ^0 = \PiL_d = \Gamma_l ^0 = 0.
\end{align}
%
In all these cases, trivially $\Gamma _f ^0$ and $\PiL _f$ can be
diagonalized simultaneously, and Higgs-mediated FCNC is avoided. 

In the flavour-aligned Two-Higgs Doublet Model (\FATHDM)~\cite{Pich:2009sp,Tuzon:2010vt} it is assumed that the $\PiL_f$ matrices are proportional to the $\Gamma_f ^0$ matrices, and we set
%
\begin{align}
  \label{eq:AlignedYuk}
  \PiL_f &= \begin{cases}
  	\xi_u^{*} \Gamma_u ^0, & \text{if }f=u, \\
  	\xi_{d,l} \Gamma_{d,l} ^0, & \text{if }f=d,l, \\
  \end{cases}  &
  &\text{with} &
  \xi_f &= \frac{\zeta_f + \tan\beta}{1 - \zeta_f\tan\beta} , 
\end{align}
%
where $\zeta _f$ are the alignment parameters which are constrained by experimental results and used in phenomenological studies. 
Note that the aligned model contains the type I, II, X and Y models as
special cases when the alignment parameters $\zeta_f$ take the values
given in \tabref{tab:zeta}.


We can summarize the fundamental Yukawa coupling matrices $\rho _f$
($f=u,d,l$) for the different \THDM\ variants and in different
parametrizations as follows:
%
\begin{align} \label{eq:rho}
  \rho_f =
  \begin{cases}
    \frac{\sqrt{2} M_f^D}{v} \zeta^{(*)}_f &\text{for type I, II, Y,
      Y (using  \tabref{tab:zeta})}
    \\
    &\text{and for the exact \FATHDM,}\\
    \frac{\sqrt{2} M_f^D}{v} \zeta^{(*)}_f +\Delta_f & \text{for the general \THDM\ in \FATHDM\ parametrization,} \\
    \frac{\Pi_f}{\cos\beta} - \frac{\sqrt{2} M_f^D}{v}\tan\beta & \text{for the general \THDM\ in $\Pi$ parametrization.} \\
  \end{cases}
\end{align}
%
Here the $\zeta_f$ are the parameters introduced in
Eq.~\eqref{eq:AlignedYuk} and \tabref{tab:zeta}, and the $(*)$
notation indicates the conjugate is present for the case $f=u$ and not
present for $f=d,l$.
\GMTCalc\ offers two parametrizations for the general \THDM. The 
``\FATHDM\ parametrization'' starts from the \FATHDM\ and allows to directly modify
the $\rho_f$ by additional matrices $\Delta_f$, which represents the
deviation from the flavour-aligned (or type I, II, X, Y) limit. 
The second ``$\Pi$ parametrization'' starts from Eq.~\eqref{eq:Piprime} and allows
to directly specify the fundamental Yukawa matrices $\Pi_f$.

\begin{table}[tb]
  \centering
  \caption{Values of the alignment parameters $\zeta_f$ for different types of Two-Higgs Doublet Models.}
  \begin{tabular}{rrrrr}
    \toprule
    & Type I & Type II & Type X & Type Y \\
    \midrule
    $\zeta_u$ & $\cot\beta$ &  $\cot\beta$ &  $\cot\beta$ &  $\cot\beta$ \\
    $\zeta_d$ & $\cot\beta$ & $-\tan\beta$ &  $\cot\beta$ & $-\tan\beta$ \\
    $\zeta_l$ & $\cot\beta$ & $-\tan\beta$ & $-\tan\beta$ &  $\cot\beta$ \\
    \bottomrule
  \end{tabular}
  \label{tab:zeta}
\end{table}

After suitable unitary transformations, the Yukawa interaction in the 
Lagrangian of Eq.\ \eqref{eq:Lyuk} takes the following form:
%
\begin{align}
  \label{eq:Lyuktransformed}
  \begin{split}
    -\mathcal{L}_\text{Yuk,int} ={}&
    H ^+ \bigg[\bar{u} \left(y_d^{\MHpm}\PR + y_u^{\MHpm}\PL \right) d + \bar{\nu} y_l^{\MHpm} \PR l \bigg] \\
    &+ \sum_{f=u,d,l} \left( \sum_{S=h,H}\,S{\bar{f}} y_f ^{S} \PR f - \imath \,A{\bar{f}} y_f ^{A} \PR f \right)
    + \hc,
  \end{split}
\end{align}
%
with the CKM matrix $\VCKM = V_u V_d^{\dagger}$ and the fermion mass
eigenstates $f=f_L + f_R$, $(f=u,d,l)$, defined in
Eq.~\eqref{eq:fmasseigen}. The coupling matrices $y_f^S$ are defined
as \cite{Cherchiglia:2016eui}:
%
\begin{align}
\label{eq:HiggsfermionCPevenh}
y_f^h &= \frac{M_f^D}{v} \sin(\beta - \alpha) + \frac{\rho_f}{\sqrt{2}} \cos(\beta -\alpha), \\
\label{eq:HiggsfermionCPevenH}
y_f^H &= \frac{M_f^D}{v} \cos(\beta - \alpha) - \frac{\rho_f}{\sqrt{2}} \sin(\beta - \alpha), \\
\label{eq:HiggsfermionCPodd}
y_f^A &= \begin{cases}
	\frac{\rho_u}{\sqrt{2}} & \text{if }f=u, \\
	- \frac{\rho_{d,l}}{\sqrt{2}} & \text{if }f=d,l, \\
\end{cases} 
\end{align}
and the coupling between the charged Higgs and each of the fermions is:
\begin{align}
\label{eq:HiggsfermionCharged}
y_f^{\MHpm} &= \begin{cases}
	-\rho_u^\dagger \VCKM & \text{if }f=u,\\
	\VCKM \rho_d & \text{if }f=d,\\
	\rho_l & \text{if }f=l, \\
\end{cases}
\end{align}
where $\rho_f$ %($f=u,d,l$) 
is given in Eq.~\eqref{eq:rho}.

\subsection{Implemented contributions to $\amu$}

Relevant \THDM\ contributions to $\amu$ arise at the one-loop and the
two-loop level. The one-loop contributions are suppressed by two
additional powers of the muon Yukawa coupling and thus of
$\order(m_\mu^4)$; they are typically subleading unless some of the
non-\SM-like Higgs bosons have very small mass around a few GeV. The
two-loop contributions can be classified into fermionic and bosonic
contributions. They arise at the  $\order(m_\mu^2)$ and are typically
dominant. An important subclass  of two-loop contributions are the
so-called Barr-Zee diagrams, which contain a $\gamma\gamma$--Higgs
one-loop subdiagram. These diagrams have been studied extensively
\cite{Barr:1990vd,Chang:2000ii,Cheung:2001hz,Wu:2001vq,Krawczyk:2002df} and fully
calculated in Ref.\ \cite{Ilisie:2015tra}. The full set of two-loop
bosonic and fermionic diagrams (at $\order(m_\mu^2)$) has been
computed in Ref.\ \cite{Cherchiglia:2016eui} (see also
Ref.\ \cite{Cherchiglia:2017uwv} for further phenomenological
discussions of the individual contributions).

\GMTCalc\ implements the full set of one-loop \BSM\ contributions $\amu^\ol$ of
$\order(m_\mu^4)$ and the two-loop \BSM\ contributions $\amu^\tl$ of
$\order(m_\mu^2)$ in the \THDM\ from Ref.\ \cite{Cherchiglia:2016eui}.  The full
\BSM\ contribution in the \THDM\ (i.e.\ the difference between the
\THDM\ and the \SM\ contributions) calculated by \GMTCalc\ is therefore
the sum
%
\begin{align}
  \amuBSM=\amu^\ol+\amu^\tl.
\end{align}
%
In the following subsections we provide the explicit analytic results
implemented in \GMTCalc.

\subsubsection{One-loop contributions}

The one-loop \BSM\ contributions to $\amu^\ol$ in the \THDM\ is of
$\order(m_\mu^4)$ and are given by
%
\begin{align}
\begin{split}
  \amu^\ol = \frac{1}{8\pi^2} \Bigg\{
     & \sum_{i=1}^3 \Bigg[
         \sum_{S=h,H,A} \frac{m_\mu^2}{m_S^2} A_S(i, m_l, m_S^2, y_l^S)
         + \frac{m_\mu^2}{m_{H^\pm}^2} A_{H^\pm}(i, 0, m_{H^\pm}^2, y_l^{H^\pm})
     \Bigg] \\
     & - \frac{m_\mu^2}{m_\hSM^2} A_h(2, m_l, m_\hSM^2, \id)
  \Bigg\},
\end{split}\label{eq:amu1L}%
\end{align}
%
with $m_l = (m_e, m_\mu, m_\tau)$, $m_\nu = (m_{\nu_e}, m_{\nu_\mu}, m_{\nu_\tau})$ and
%
\begin{align} 
  \label{eqn:onelooph}
  A_h(i,m_l,m_S^2,y_l^S) ={}& A_S^+(i,m_l,m_S^2,y_l^S), \\
  \label{eqn:oneloopH}
  A_H(i,m_l,m_S^2,y_l^S) ={}& A_S^+(i,m_l,m_S^2,y_l^S), \\
  \label{eqn:oneloopA}
  A_A(i,m_l,m_S^2,y_l^S) ={}& A_S^-(i,m_l,m_S^2,y_l^S), \\
  \nonumber
  A_S^\pm(i,m_l,m_S^2,y_l^S) ={}&
    \frac{1}{24} \left( \left|(y_l^S)_{i2}\right|^2 + \left|(y_l^S)_{2i}\right|^2 \right) F_1^C\left(\frac{(m_l)^2_i}{m_S^2}\right) \\
    \label{eqn:oneloopS}
    &\pm \frac{1}{3} \real\left[(y_l^S)_{i2}^* (y_l^S)_{2i}^*\right] \frac{(m_l)_i}{m_\mu} F_2^C\left(\frac{(m_l)^2_i}{m_S^2}\right), \\
  \label{eqn:oneloopHp}
  A_{H^\pm}(i, m_\nu, m_S^2, y_l^S) ={}&
    -\frac{1}{48} \left|(y_l^S)_{i2}\right|^2 \left[ F_1^N \left(\frac{m_{\nu_\mu}^2}{m_S^2}\right) + F_1^N\left(\frac{(m_\nu)_i^2}{m_S^2}\right) \right].
\end{align}
%
Note, that the one-loop contribution from the \SM\ Higgs boson $\hSM$
is subtracted in Eq.~\eqref{eq:amu1L} and is therefore not included in
$\amu^\ol$.  The loop functions $F_1^C$, $F_2^C$ and $F_1^N$ are given
by
%
\begin{align}
  F_1^C(x) &= \frac{2}{(x - 1)^4}\left(2 + 3x - 6x^2 + x^3 + 6x\ln x\right), & F_1^C(0)&=4, & F_1^C(1)&=1, \\
  F_2^C(x) &= \frac{3}{2(1 - x)^3}\left(-3 + 4x - x^2 - 2\ln x\right), & F_2^C(1)&=1, & F_2^C(\infty)&=0, \\
  F_1^N(x) &= \frac{2}{(x - 1)^4}\left(1 - 6x + 3x^2 + 2x^3 - 6x^2\ln x\right), & F_1^N(0)&=2, & F_1^N(1)&=1, & F_1^N(\infty)=0.
\end{align}


\subsubsection{Two-loop contributions}

The two-loop contributions of $\order(m_\mu^2)$ are divided into
fermionic and bosonic loop contributions according to Ref.\
\cite{Cherchiglia:2016eui} as
%
\begin{align}
  \amu^{\tl} = \amuB + \amuF ,
  \label{eq:amu2L}
\end{align}
%
where the very small contribution from the shift of the Fermi constant,
$\amu^{\Delta r\text{-shift}}$, is neglected.  We generalize the
fermionic two-loop contributions from Ref.\ \cite{Cherchiglia:2016eui}
in the general \THDM\ to the case of CKM mixing as follows:
%
\begin{align}
  \amuF &= \amu^{\text{F,N}} + \amu^{\text{F,C}} , \\
  \amu^{\text{F,N}} &= \sum_{f=u,d,l} \sum_{i=1}^3 \frac{\aem^2 m_\mu^2}{4\pi^2 m_W^2 s_W^2} \left[
  \sum_{S=h,H,A} f_f^S(m_S,(m_f)_i) \frac{\real\left[(y_f^S)_{ii}^* (y_l^S)_{22}\right] v^2}{(m_f)_i m_\mu}
  - f_f^{\hSM}(m_{\hSM},(m_f)_i) \right], \label{eq:amu2LFN} \\
  \amu^{\text{F,C}} &=  \sum_{i,j=1}^3
  \frac{\aem^2 m_\mu^2}{32\pi^2 m_W^2 s_W^4}
  \Bigg[
    f_u^{H^\pm}(m_{H^\pm},(m_u)_i,(m_d)_j)
    \frac{\real \left[(y_u^{H^\pm})_{ij}^* (\VCKM)_{ij}(y_l^{H^\pm})_{22}\right] v^2}{2 (m_u)_i
      m_\mu}
    \nonumber\\
    &\qquad\qquad\qquad\qquad\qquad
    +f_d^{H^\pm}(m_{H^\pm},(m_d)_j,(m_u)_i)
    \frac{\real \left[(y_d^{H^\pm})_{ij}^* (\VCKM)_{ij}(y_l^{H^\pm})_{22}\right] v^2}{2 (m_d)_j
      m_\mu}
    \nonumber\\
    &\qquad\qquad\qquad\qquad\qquad
    +f_l^{H^\pm}(m_{H^\pm},(m_l)_i,0)
    \frac{\real\left[ (y_l^{H^\pm})_{ij}^* \delta_{ij}(y_l^{H^\pm})_{22}\right] v^2}{2 (m_l)_i
      m_\mu}
    \Bigg].
\end{align}

Note, that the two-loop
contribution from the \SM\ Higgs boson $\hSM$ is subtracted in
Eq.~\eqref{eq:amu2LFN} and is therefore not included in $\amu^\tl$.
The loop functions $f_f^S$ ($S=h,H,A,\hSM$) are defined as:
%\cite[Sec.~3.4){Cherchiglia:2016eui}.
\begin{align}
	\label{eqn:twoloopfS}
	f_f^S(m_S,m_f) ={}& q_f^2 N^c_f \frac{m_f^2}{m_S^2} \F_S (m_S,m_f)
	- q_f N^c_f \frac{g^l_v g^f_v}{s_W^2 c_W^2} \frac{m_f^2}{m_S^2- m_Z^2} \left[\F_S (m_S,m_f) - \F_S (m_Z,m_f)\right], \\
	\label{eqn:twoloopFS}
	\F_S(m_S,m_f) ={}& \begin{cases}
		-2 + \ln\bigg(\frac{m_S^2}{m_f^2}\bigg) - \bigg(\frac{m_S^2-2m_f^2}{m_S^2}\bigg) \frac{\Phi(m_S^2,m_f^2,m_f^2)}{m_S^2-4m_f^2}, & S=h,H,\hSM,\\
		\frac{\Phi(m_S^2,m_f^2,m_f^2)}{m_S^2-4m_f^2}, & S=A,\\
	\end{cases}
\end{align}
%
where $N^c_f = (1,3,3)$, $g^f_v = T^3_f/2 - s_W^2 q_f$,
$T^3_f = (-1/2,-1/2,1/2)$ and $q_f=(-1,-1/3,2/3)$ for $f=(l,d,u)$.
The loop function $f_f^{H^\pm}$ is defined as:
%
\begin{align}
	\label{eqn:twoloopHp}
	f_f^{H^\pm}(m_{H^\pm},m_f,m_{f'}) = \begin{cases}
		\frac{m_l^2}{m_{H^\pm}^2-m_W^2} \left[\F_l^{H\pm}\bigg(\frac{m_l^2}{m_{H\pm}^2}\bigg) - \F_l^{H^\pm}\bigg(\frac{m_l^2}{m_W^2}\bigg)\right], & f=l, f'=\nu, \\
		3\frac{m_d^2}{m_{H^\pm}^2-m_W^2} \left[\F_d^{H\pm}\bigg(\frac{m_d^2}{m_{H\pm}^2},\frac{m_u^2}{m_{H\pm}^2}\bigg) - \F_d^{H^\pm}\bigg(\frac{m_d^2}{m_W^2},\frac{m_u^2}{m_W^2}\bigg)\right], & f=d, f'=u, \\
		3\frac{m_u^2}{m_{H^\pm}^2-m_W^2} \left[\F_u^{H\pm}\bigg(\frac{m_d^2}{m_{H\pm}^2},\frac{m_u^2}{m_{H\pm}^2}\bigg) - \F_u^{H^\pm}\bigg(\frac{m_d^2}{m_W^2},\frac{m_u^2}{m_W^2}\bigg)\right], & f=u, f'=d,
	\end{cases}
\end{align}
%
where $f'_j$ is the $\SU(2)_L$ partner of generation $j$ of the
fermion $f_i$ from generation $i$, and
%
\begin{align}
	\label{eqn:twoloopFlHp}
	\F_l^{H^\pm}(x_l) ={}& x_l + x_l (x_l-1) \left[\dilog\bigg(1-\frac{1}{x_l}\bigg) -\frac{\pi^2}{6}\right] + \left(x_l-\frac{1}{2}\right)\ln(x_l), \\ 
	\nonumber
	\F_d^{H^\pm}(x_d,x_u) ={}& -(x_u+x_d) + \left[\frac{\overline{c}}{y} - \frac{c(x_u-x_d)}{y}\right] \Phi(x_d,x_u,1) \\ 
	\nonumber
	&+ c\left[\dilog(1-x_d/x_u) - \frac{1}{2} \ln{(x_u)} \ln{(x_d/x_u)}\right] \\
	\label{eqn:twoloopFdHp}
	&+ (s+x_d)\ln{(x_d)} + (s-x_u)\ln{(x_u)}, \\ 
	\nonumber
	\F_u^{H^\pm}(x_d,x_u) ={}& \F_d^{H^\pm}(x_d,x_u) (q_u \rightarrow 2+q_u, q_d \rightarrow 2+q_d) \\
	\label{eqn:twoloopFuHp}
	&-\frac{4}{3} \frac{(x_u-x_d-1)}{y} \Phi(x_d,x_u,1) - \frac{1}{3} \left[\ln^2{(x_d)} - \ln^2{(x_u)}\right],
\end{align}
%
and
%
\begin{align}
  c &= (x_u - x_d)^2 - q_u x_u + q_d x_d, \\
  \overline{c} &= (x_u - q_u) x_u - (x_d + q_d) x_d, \\
  y &= (x_u - x_d)^2 - 2 (x_u + x_d) + 1, \\
  s &= \frac{q_u+q_d}{4}.  
\end{align}
%
The loop function $\Phi(m_1^2,m_2^2,m_3^2)$ \cite{DAVYDYCHEV1993123}
is defined as:
%
\begin{align}
	\Phi(m_1^2,m_2^2,m_3^2) &= \frac{\lambda}{2} \bigg[2\ln{(\alpha_+)}\ln{(\alpha_-)} - \ln{\bigg(\frac{m_1^2}{m_3^2}\bigg)}\ln{\bigg(\frac{m_2^2}{m_3^2}\bigg)} - 2\dilog{(\alpha_+)} - 2\dilog{(\alpha_-)} + \frac{\pi^2}{3}\bigg], \\
	\alpha_{\pm} &= \frac{m_3^2 \pm m_1^2 \mp m_2^2 - \lambda}{2m_3^2}, \\
	\lambda &= \sqrt{m_1^4 + m_2^4 + m_3^4 - 2m_1^2 m_2^2 - 2m_2^2 m_3^2 -2 m_3^2 m_1^2}.  
\end{align}

The bosonic two-loop contributions to the general \THDM\ are composed of three parts as follows:
%
\begin{equation} \label{eqn:twoloopB}
	\amuB = \amu^{\text{B,EW add}} + \amu^{\text{B,Yuk}} + \amu^{\text{B,non-Yuk}}.
\end{equation}
%
The two terms $\amu^{\text{B,EW add}}$ and $\amu^{\text{B,nonYuk}}$
correspond to diagrams with the \SM-like Higgs boson and diagrams
without the new Yukawa couplings, respectively. They are typically
subdominant, and full details on their definition and phenomenological
impact are given in 
Refs.~\cite{Cherchiglia:2016eui} and \cite{Cherchiglia:2017uwv}. 
The Yukawa contribution is given by
%
\begin{align} \label{eqn:twoloopYuk}
	\nonumber
	\amu^{\text{B,Yuk}} ={}& \frac{\aem^2}{574\pi^2c_W^4s_W^4} \frac{m_\mu^2}{m_Z^2} \bigg\{ a_{0,0}^{0} + a_{0,z}^{0} \left(\tan\beta-\frac{1}{\tan\beta}\right) \zeta_l + a_{5,0}^{0} \Lambda_5 + a_{5,z}^{0} \left(\tan\beta-\frac{1}{\tan\beta}\right) \Lambda_{567} \zeta_l \\
	&+ \left[a_{0,0}^{1} \left(\tan\beta-\frac{1}{\tan\beta}\right) + a_{0,z}^{1} \zeta_l + a_{5,0}^{1} \left(\tan\beta-\frac{1}{\tan\beta}\right) \Lambda_{567} + a_{5,z}^{1} \Lambda_5 \zeta_l\right] \cos(\beta - \alpha)\bigg\}, \\
	\Lambda_5 ={}& \frac{2m_{12}^2}{v^2\sin\beta\cos\beta}, \\
\label{eq:lambda567}        \Lambda_{567}={}& \Lambda_5+\frac{1}{\tan\beta-\frac{1}{\tan\beta}}\left(\frac{\lambda_6}{\sin^2\beta}-\frac{\lambda_7}{\cos^2\beta}\right),
\end{align}	
%
where the equations for $a_{\lambda,t}^{\eta}$ (with the common
prefactor put in the front of the above Eq.~\eqref{eqn:twoloopYuk})
are given in the appendix of
Ref.~\cite{Cherchiglia:2016eui}. Compared to this reference, the
expression has been generalized to include $\lambda_6$ and
$\lambda_7$.\footnote{%
  These two Higgs potential parameters only enter via triple Higgs
  couplings, and they enter only via the combination $\Lambda_{567}$
  defined in Eq.\ \eqref{eq:lambda567}. The suitable generalizations of Eqs.\ (3.25)--(3.26) of
  Ref.\ \cite{Cherchiglia:2016eui} for the required triple Higgs
  couplings can be obtained e.g.\ from formulas in the Appendix of
  Ref.\ \cite{Gunion:2002zf} and can simply be written as
    \begin{align}
g_{\Mh,H^\pm,H^\mp} \propto& \left\{\vac\,\left( \Lambda_5 - \frac{\MMh^2}{\vac^2} - 2\frac{\MMHpm^2}{\vac^2}\right)%\right.\nonumber\\
%& \left. \,\,
+\, \eta\,\left(\tan\beta - \frac{1}{\tan\beta}\right)\frac{\vac}{2}\left(2\,\frac{\MMh^2}{\vac^2} - \Lambda_{567}\right)\right\},\\
g_{\MH,H^\pm,H^\mp} \propto& \left\{
\left(\tan\beta - \frac{1}{\tan\beta }\right)\frac{\vac}{2}\left( \Lambda_{567} -  2\,\frac{\MMH^2}{\vac^2} \right)%\right.\nonumber\\
%&\left.\,\,
+ \eta \vac\,\left( \Lambda_5 - \frac{\MMH^2}{\vac^2} - 2\frac{\MMHpm^2}{\vac^2}\right)\right\}.
    \end{align}
    This structure clarifies the appearance of $\Lambda_{567}$ in
    Eq.\ \eqref{eqn:twoloopYuk}.
}
These bosonic contributions are implemented in the realistic
approximation of small $\cos(\beta-\alpha)$, and only terms up to
linear order in $\cos(\beta-\alpha)$ are taken into
account.  In this scenario the boson $h$ has the tree-level couplings of the \SM\ Higgs boson \cite{Gunion:2002zf}.  
Furthermore, the bosonic contributions are derived only for
the flavour-aligned \THDM. Referring to the different cases of Yukawa
couplings in Eq.\ \eqref{eq:rho}, the bosonic corrections apply for
the cases of the discrete symmetries, for the \FATHDM, and also for
the general \THDM\ in \FATHDM\ parametrization (assuming the $\Delta_f$
matrices are small). In contrast,  we set
$\zeta_l=0$ in Eq.~\eqref{eqn:twoloopYuk} in the case of the general
\THDM\ in $\Pi$ parametrization since in this case the bosonic
corrections are not applicable in this form.

The bosonic two-loop contributions
are also available in the \GMTCalc\ source code and in the form of a
\mathematica\ file.  

\subsubsection{Running couplings}
\label{sec:running_couplings}

Among the fermionic two-loop contributions, the diagrams with an
internal top quark, bottom quark or tau lepton loop give the dominant
contribution to $\amuBSM$.  These diagrams are proportional to the values
of the fermion masses in the loop.  So long as there is no three-loop
calculation available in the \THDM, it is formally irrelevant which
renormalization scheme to use for the fermion masses in the loop.  In \GMTCalc\ two possible
definitions of these fermion masses are implemented:
%
\begin{align}
  \text{Input masses:}&   \qquad m_t, m_b^\MS(m_b^\MS), m_\tau
  \label{eq:input_masses} \\
  \text{Running masses:}& \qquad m_t^\MS(Q), m_b^\MS(Q), m_\tau^\MS(Q)
  \label{eq:running_masses}
\end{align}
%
In the ``input masses'' scheme, the top quark pole mass $m_t$, the
$\MS$ bottom quark mass in the \SM\ with five active quark flavours,
$m_b^\MS$, at the renormalization scale $Q=m_b^\MS$, and the tau
lepton pole mass $m_\tau$ are used in the loops.  These masses are
typically used as input for spectrum generators
\cite{Skands:2003cj,Allanach:2008qq}.
%
In the ``running masses'' scheme, the running $\MS$ top quark mass
$m_t^\MS(Q)$, the $\MS$ bottom quark mass $m_b^\MS(Q)$ and the $\MS$
tau lepton mass $m_\tau^\MS(Q)$ are used in the loops.  The
renormalization scale $Q$ is set to the mass of the Higgs boson in the
Feynman diagram.  The ``running masses'' scheme is also used in
\THDMC\ \cite{Eriksson:2009ws}.  The difference between these two
schemes is shown in \secref{sec:applications}.


\subsubsection{Uncertainty estimate}

We provide an uncertainty estimate for $\amu^{\tl}$ as follows:
%
\begin{align} 
  \Damu^{\tl} &= \Damu^{\tl,\Delta r} + \Damu^{\tl,m_\mu^4} + \Damu^{\thl}, \label{eq:damu}
\end{align}
%
where
%
\begin{align}
  \Damu^{\tl,\Delta r} &= 2\times 10^{-12}, \\
  \Damu^{\tl,m_\mu^4} &= \left| \amu^{\ol}\;\Delta\aem \right|, \\
  \Damu^{\thl} &= \left| \amu^{\tl}\;\Delta\aem \right|,
\end{align}
%
and
%
\begin{align}
  \Delta\aem &= -\frac{4\aem}{\pi} \ln\left(\frac{m_\text{NP}}{m_\mu}\right),
  & m_\text{NP} &= \min\{m_H, m_A, m_{H^\pm}\}.
\end{align}
%
The term $\Damu^{\tl,\Delta r}$ accounts for the fact that the
two-loop contribution $\amu^{\Delta r\text{-shift}}$ has been
neglected in Eq.~\eqref{eq:amu2L}.  In Refs.~\cite{Lopez-Val:2012uou,Cherchiglia:2016eui} it was
shown that in the relevant parameter space
$|\amu^{\Delta r\text{-shift}}| \leq 2\times 10^{-12}$, which we use as
an upper bound in the uncertainty estimate.
%
The term $\Damu^{\tl,m_\mu^4}$ estimates missing two-loop terms of
$\order(m_\mu^4)$ using the known universal two-loop QED logarithmic
contributions \cite{Degrassi:1998es}.
%
The term $\amu^{\thl}$ is an estimate for the expected three-loop
contributions.  From experience the QED contributions are among the
largest contributions at each loop level. For this reason we use again the known universal
logarithmic QED contributions from Ref.~\cite{Degrassi:1998es} to 
provide an estimate for the unknown three-loop contributions.

