 \section{Introduction}

 The anomalous magnetic moment of the muon, $\amu$, is one of the most
 important observables in particle physics.  Its importance stems from
 the fact that it is one of the most precisely measured physical 
 quantities and that it is very sensitive to new
 physics as well as the strong, weak and electromagnetic interactions
 of the Standard Model (\SM).  The significance of $\amu$ has increased further
 in the wake of the recent release of the first results from the
 Fermilab Muon $g-2$ experiment \cite{PhysRevLett.126.141801}.  This collaboration
 found, in combination with the results from the Brookhaven National
 Laboratory \cite{Tanabashi2018}, an experimental value of
%
\begin{equation} \label{eqn:Gm2Exp}
	\amu^\text{Exp} = (11659206.1\pm4.1)\times10^{-10}.
\end{equation}
%
The \SM\ prediction from the Muon $g-2$ Theory Initiative White Paper \cite{aoyama2020anomalous} is
%
\begin{equation} \label{eqn:Gm2SM}
	\amu^{\SM} = (11659181.0\pm4.3)\times10^{-10},
\end{equation}
%
which combines quantum electrodynamic contributions \cite{Aoyama:2012wk,Aoyama:2019ryr}, 
electroweak contributions \cite{Czarnecki:2002nt,Gnendiger:2013pva}, 
hadronic vacuum-polarization \cite{Davier:2017zfy,Keshavarzi:2018mgv,Colangelo:2018mtw,Hoferichter:2019gzf,Davier:2019can,Keshavarzi:2019abf,Kurz:2014wya}
and light-by-light contributions \cite{Melnikov:2003xd,Masjuan:2017tvw,Colangelo:2017fiz,Hoferichter:2018kwz,Gerardin:2019vio,Bijnens:2019ghy,Colangelo:2019uex,Pauk:2014rta,Danilkin:2016hnh,Jegerlehner:2017gek,Knecht:2018sci,Eichmann:2019bqf,Roig:2019reh,Blum:2019ugy,Colangelo:2014qya}.\footnote{%
  As discussed extensively in the Theory Initiative White Paper
  \cite{aoyama2020anomalous}, the proposed \SM\ prediction does not use
  lattice gauge theory evaluations of the hadronic vacuum
  polarization. The lattice world average evaluated in
  Ref.\ \cite{aoyama2020anomalous}, based on \cite{Chakraborty:2017tqp,Borsanyi:2017zdw,
Blum:2018mom,Giusti:2019xct,Shintani:2019wai,Davies:2019efs,Gerardin:2019rua,Aubin:2019usy,Giusti:2019hkz}, is compatible
  with the data-based result 
\cite{Davier:2017zfy,Keshavarzi:2018mgv,Colangelo:2018mtw,Hoferichter:2019gzf,Davier:2019can,Keshavarzi:2019abf,Kurz:2014wya}, has a higher
  central value and larger uncertainty. However more recent lattice results
  are obtained in Refs.\ \cite{Borsanyi:2020mff,Lehner:2020crt}, and
  in particular Ref.\ \cite{Borsanyi:2020mff} obtains a result with smaller
  uncertainty, which would shift the \SM\ prediction for $\amu$ closer to the
  experimental value. Scrutiny of these results is
  ongoing (see e.g.\ Ref.\ \cite{Colangelo:2020lcg}) and further
  progress can be expected.\label{footnoteLQCD}
  }
The experimental measurement differs from the \SM\ prediction by $4.2\sigma$ which suggests a beyond the Standard
Model (\BSM) contribution of
%
\begin{equation}
	\amuBSM = (25.1\pm5.9)\times10^{-10}.
        \label{eq:amuBSM}
\end{equation} 
%
The high precision in both the experimental measurement and the \SM\ theoretical prediction requires \BSM\ contributions to be known with a similar level of accuracy. 
%QED\cite{Aoyama:2012wk,Aoyama:2019ryr}
%EW\cite{Czarnecki:2002nt,Gnendiger:2013pva}
%HVP\cite{Davier:2017zfy,Keshavarzi:2018mgv,Colangelo:2018mtw,Hoferichter:2019gzf,Davier:2019can,Keshavarzi:2019abf,Kurz:2014wya}
%HLBL\cite{Melnikov:2003xd,Masjuan:2017tvw,Colangelo:2017fiz,Hoferichter:2018kwz,Gerardin:2019vio,Bijnens:2019ghy,Colangelo:2019uex,Pauk:2014rta,Danilkin:2016hnh,Jegerlehner:2017gek,Knecht:2018sci,Eichmann:2019bqf,Roig:2019reh,Blum:2019ugy,Colangelo:2014qya}

The Two-Higgs Doublet Model (\THDM) is one of the simplest and most widely studied extensions of the \SM\ 
(see e.g.\ Ref.\ \cite{Branco:2011iw} for a review or the relevant chapters of Ref.\ \cite{Gunion:1989we}). 
Although the \THDM\ has been studied for many decades and the only Higgs boson discovered is closely \SM-like \cite{ATLAS:2012yve,CMS:2012qbp},
% we now have an experimental handle on the Higgs sector following the discovery of the Higgs boson, 
interest in this model has not waned, and recent progress has been
achieved e.g.\ on LHC interpretations 
\cite{Haber:2015pua,Baglio:2014nea,Eberhardt:2013uba,Chowdhury:2015yja,Chowdhury:2017aav}, 
B-physics \cite{Misiak:2017bgg,Hu:2016gpe,Li:2014fea,Enomoto:2015wbn,Arnan:2017lxi}, 
theoretical constraints \cite{Barroso:2013awa,BhupalDev:2014bir,Kanemura:2015ska,Draper:2020tyq,Gori:2017qwg}, 
electroweak phase transitions in the early universe
\cite{Basler:2016obg,Basler:2017uxn,Basler:2019iuu,Kainulainen:2019kyp,Basler:2021kgq},
precision calculations of Higgs decays
\cite{Krause:2016oke,Krause:2016xku,Altenkamp:2017kxk,Altenkamp:2017ldc,Altenkamp:2018hrq,Denner:2018opp,Kanemura:2018yai,Krause:2019qwe,Aiko:2021can}.
Remarkably, the \THDM\ is one of the very few single field extensions of the
Standard Model that can explain the deviation between
$\amu^\text{Exp}$ and $\amu^{\SM}$ \cite{Athron:2021iuf} while
satisfying existing constraints from collider physics searches and
other observables.

A second Higgs doublet that can couple to \SM\ fermions generically induces large tree-level flavour changing neutral currents at odds with experiment.  
Due to this \THDM s are often classified according to the discrete symmetries imposed to conserve flavour, giving four types \cite{Barger:1989fj}: type I \cite{Haber:1978jt}, type II \cite{Hall:1981bc}, type X (often called the lepton specific \THDM) \cite{Barnett:1983mm,Barnett:1984zy} and type Y (sometimes refereed to as the flipped \THDM) \cite{Barger:2009me,Logan:2010ag}.  
Large flavour changing neutral currents can also be avoided by assuming an alignment
in flavour space between the Yukawa matrices of the two doublets, giving the so-called
flavour-aligned \THDM\ (\FATHDM) \cite{Pich:2009sp,Tuzon:2010vt}.
After the first results of the LHC, Ref.\ \cite{Broggio:2014mna}
systematically investigated the phenomenology of all \THDM\ versions
with discrete symmetries and showed that among them, only the type X
variant is able to provide significant contributions to $\amu$. The
type X explanation  of the deviation between $\amu^\text{Exp}$ and $\amu^{\SM}$
has been further explored in Refs.\
\cite{Wang:2014sda,Abe:2015oca,Chun:2015xfx,Chun:2015hsa,Chun:2016hzs,Wang:2018hnw,Chun:2019oix,Chun:2019sjo,Keung:2021rps,Ferreira:2021gke,Han:2021gfu,Eung:2021bef,Jueid:2021avn,Dey:2021pyn}.
The more general flavour-aligned \THDM\ and its contributions to
$\amu$ were studied in
Refs.\ \cite{Ilisie:2015tra,Han:2015yys,Cherchiglia:2016eui,Cherchiglia:2017uwv,Li:2020dbg,Athron:2021iuf}. Other variants, including more general 
realisations that allow for Higgs-mediated flavour violation were investigated in Refs.\
\cite{Crivellin:2015hha,Omura:2015nja,Iguro:2019sly,Jana:2020pxx,Hou:2021sfl,Hou:2021qmf,Hou:2021wjj,Atkinson:2021eox}.

Phenomenological investigations of \BSM\ physics are greatly enhanced
by the use of precise software tools.  These tools can automate the
calculation of precision corrections, or provide numerical methods to
quickly and reliably solve related problems.  For example in the
\THDM\ one is often concerned that a particular benchmark point
respects measured limits of electroweak oblique parameters, or that
the new couplings and bosons do not provide contributions to Higgs
decays which violate collider constraints.  
For the \THDM\ a widely used software package is
\THDMC\ \cite{Eriksson:2009ws}, which provides calculations of the
spectrum, decays, oblique $S$, $T$ and $U$ parameters as well as a
calculation of the \THDM\ new physics contributions to $\amu$.  There
also exists the tool \ScannerS\ \cite{Muhlleitner:2020wwk} which can
place theoretical, experimental, dark matter, and electroweak phase
transition constraints on extended scalar sectors, including the
\THDM.  A more precise calculation of the decays is available from the
\THDM\ decay dedicated code \THDECAY\ \cite{Krause:2018wmo}, while
\PROPHECYfourF\ \cite{Denner:2019fcr} provides a package which focuses
on calculating $h\rightarrow WW/ZZ\rightarrow4f$ decays.
Additionally, the tools
\HiggsBounds\ \cite{Bechtle:2008jh,Bechtle:2011sb,Bechtle:2012lvg,Bechtle:2013wla,Bechtle:2015pma}
and \HiggsSignals\ \cite{Stal:2013hwa,Bechtle:2013xfa,Bechtle:2014ewa}
allow one to place constraints on the Higgs sectors of \BSM\ physics
through the measured behaviour of Higgs bosons from collider search
experiments.



Higher precision calculations for the spectrum, decays and other
observables in the \THDM\ are also available for arbitrary
user-defined extensions of the \SM\ through codes such as
\SARAH/\spheno\ \cite{Staub:2009bi,Staub:2010jh,Staub:2012pb,Staub:2013tta,Porod:2003um,Porod:2011nf}
and
\FS\ \cite{Athron:2014yba,Athron:2016fuq,Athron:2017fvs,Athron:2021kve}
(with decays recently added in \FD\ \cite{Athron:2021kve}) where model
files for the \THDM\ are already distributed.  Additionally, both
packages provide one-loop contributions to $\amuBSM$.  For two-loop
contributions to $\amuBSM$ in the \MSSM, \FS\ links to the dedicated
tool \GMTCalc\ \cite{Athron:2015rva}. For the \THDM\ there was no such
option until now.  Here we extend \GMTCalc\ with the \THDM\ to provide
a program which includes state of the art two-loop level contributions
of Ref.\ \cite{Cherchiglia:2016eui} to the anomalous magnetic moment
of the muon.


The \THDM\ contributions implemented in \GMTCalc\ version 2 include
the one-loop contributions, the two-loop fermionic corrections
(including the well-known Barr-Zee diagrams), and the complete set of
bosonic two-loop corrections. The one-loop contributions are of the
order $\order(m_\mu^4)$ and therefore usually subdominant. The
two-loop contributions arise  at
$\order(m_\mu^2)$ and are implemented at this order. 
Ref.\ \cite{Cherchiglia:2016eui} has assumed the flavour-aligned
\THDM\ with vanishing couplings $\lambda_{6,7}$, but here we relax
this assumption and allow the more general case.
The \THDM\ version of \GMTCalc~2 allows the user to input
deviations from the aligned limit, as well as select any one of the
\THDM\ types I, II, X or Y.  Just like version 1, \GMTCalc~2
allows the user to input parameter information using an SLHA-like
\cite{Skands:2003cj,Allanach:2008qq} input file.  We also
provide interfaces in C, C++, \python\ and \mathematica\ to make it easy
to link to other public codes.
%
Furthermore, \GMTCalc~2 (hereafter \GMTCalc) can be used as a standalone tool for studies
of $\amu$ in the \THDM, or to explore the \THDM\ phenomenology more
broadly it can be used in combination with other codes via the SLHA
interface.  For example, \GMTCalc\ can be called alongside \FS,
or in combination with other standalone tools like \THDECAY\ or
alongside the \THDMC\ package, replacing its native calculation of the
anomalous magnetic moment of the muon.  The \MSSM\ calculation is
already available in
\GAMBIT\ \cite{GAMBIT:2017yxo,GAMBITModelsWorkgroup:2017ilg} and it
should be straightforward to extend this to also use the \THDM\
calculation in future versions.

The rest of this paper is divided into the following sections.
\secref{sec:model} provides a pedagogical introduction to the \THDM, giving
the Lagrangian both before and after electroweak symmetry breaking,
and defining the Yukawa couplings in the various types of the \THDM.
There we give the one- and two-loop contributions to $\amuBSM$, and
the uncertainty estimate for the calculation.  
\secref{sec:implementation} describes various ways to use the \THDM\
in \GMTCalc.  It also shows how to interface \GMTCalc\ using C++,
C, \python\ and \mathematica.  Finally, \secref{sec:applications}
demonstrates various possible ways \GMTCalc\ can be applied to
calculate the \BSM\ contributions to the anomalous magnetic moment of
the muon in the \THDM.
